# DistAIX FBScomponents Library

[![pipeline status](https://git.rwth-aachen.de/acs/research/swarmgrid/fbs-components/badges/master/pipeline.svg)](https://git.rwth-aachen.de/acs/research/swarmgrid/fbs-components/commits/master)
[![coverage report](https://git.rwth-aachen.de/acs/research/swarmgrid/fbs-components/badges/master/coverage.svg)](https://git.rwth-aachen.de/acs/research/swarmgrid/fbs-components/commits/master)

The DistAIX FBScomponents Library provides the electrical models and solving functions used by DistAIX.

## Dependencies
The library has the following dependencies
- GNU Scientific Library 
- CMake 

## Compiling
Use CMake to compile the library on your system by invoking the ``build.sh`` script in the build folder.
Use the ``cleanup.sh`` script to remove all temporary CMake files.

## Usage
The library is built and used automatically by DistAIX.

### Code Documentation
You can use the Doyxfile provided in the `docs` folder to create an HTML documentation of the code.

## Copyright

2022, Institute for Automation of Complex Power Systems, EONERC  

## License
This project is released under the terms of the [GPL version 3](COPYING.md).

```
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
```

For other licensing options please consult [Prof. Antonello Monti](mailto:amonti@eonerc.rwth-aachen.de).

## Contact

- [Felix Wege](mailto:felix.wege@eonerc.rwth-aachen.de)

[Institute for Automation of Complex Power Systems (ACS)](http://www.acs.eonerc.rwth-aachen.de)  
[EON Energy Research Center (EONERC)](http://www.eonerc.rwth-aachen.de)  
[RWTH University Aachen, Germany](http://www.rwth-aachen.de)  
