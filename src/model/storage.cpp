/**
 * This file belongs to DistAIX FBScomponents
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/

#include "model/storage.h"
#include "fbs_config.h"

/*!
 * \brief Construct a new Storage:: Storage object
 * \param _C        storage capacity in Wh
 * \param _P_max    maximum charging and discharging power
 * \param _soc_init initial soc
 */
Storage::Storage(double _stepsize, double _C, double _P_max_in, double _P_max_out, double _soc_init)
    : FBS_model(_stepsize, 0, 0), C(_C), P_max_in(_P_max_in), P_max_out(_P_max_out), soc(_soc_init)
{
}

/*!
 * \brief proceed to next time step
 * \param t next time step
 */
void Storage::step(double t)
{
    t_prev = t_next;
    t_next = t;
    if(first_step) {
        first_step = false;
    }
    return;
}

/*!
 * \brief update soc
 * \param P         power (charging or discharging)
 */
void Storage::update(double P)
{
    double P_min, P_max;
    get_power_limits(P_min, P_max);
    if (P > P_max) {
        P = P_max;
    } else if (P < P_min) {
        P = P_min;
    }

    if(C > 0) { // if storage available
        soc = soc + (P * stepsize / 3600) / C;
    }
    else
    {
        soc = 0;
    }

    if (soc < 0) {
        soc = 0;
    } else if (soc > 1) {
        soc = 1;
    }
    return;
}

/*!
 * \brief get currently stored energy
 * \return energy in Wh
 */
double Storage::get_energy()
{
    return C * soc;
}

/*!
 * \brief get energy lack
 * \return energy lack in Wh
 */
double Storage::get_energy_lack()
{
    return C * (1.0 - soc);
}

/*!
 * \brief get SOC
 * \return SOC 
 */
double Storage::get_soc()
{
    return soc;
}

/*!
 * \brief get operation range of storage
 * \param P_min [out]   minimum power
 * \param P_max [out]   maximum power
 */
void Storage::get_power_limits(double &P_min, double &P_max)
{
    P_max = ((1- soc) * C * 3600) / stepsize;
    if (P_max > P_max_in) {
        P_max = P_max_in;
    }
    P_min = -(soc * C * 3600) / stepsize;
    if (P_min < -P_max_out) {
        P_min = -P_max_out;
    }
    return;
}