/**
 * This file belongs to DistAIX FBScomponents
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/

#include "model/line.h"

/*!
 * \brief Construct a new Line::Line object
 * \param _v_nom nominal voltage
 * \param _S_r rated power
 */
Line::Line(double _stepsize, double _v_nom, double _S_r, double _R, double _X, double _G, double _B) 
    : Two_port_grounded(_stepsize, _v_nom, _S_r), R(_R), X(_X), G(_G), B(_B)
{
    i1_leak_re = 0;
    i1_leak_im = 0;
    i2_leak_re = 0;
    i2_leak_im = 0;
}

/*!
 * \brief get sum of leakage currents
 * \param _i_leak_re [out] real part of leakage current in A
 * \param _i_leak_im [out] imaginary part of leakage current in A
 */
void Line::get_leakage_current(double &_i_leak_re, double &_i_leak_im)
{
    _i_leak_re = i1_leak_re + i2_leak_re;
    _i_leak_im = i1_leak_im + i2_leak_im;
    return;
}