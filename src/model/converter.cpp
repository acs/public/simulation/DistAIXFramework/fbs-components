/**
 * This file belongs to DistAIX FBScomponents
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/

#include "model/converter.h"
#include <cmath>

/*!
 * \brief Construct a new Converter:: Converter object
 * \param _v_nom    nominal voltage
 * \param _S_r      rated power
 * \param _pf_min   minimum power factor
 */
Converter::Converter(double _stepsize, double _v_nom, double _S_r, double _pf_min)
    : One_port_grounded(_stepsize, _v_nom, _S_r), pf_min(_pf_min)
{
}

/*!
 * \brief set the target power for converter control
 * \param _P_target active power target
 * \param _Q_target reactive power target
 */
void Converter::set_target_power(double &_P_target, double _Q_target)
{
    /* check limits of converter and set target values */
    if (_P_target > S_r) {
        P_target = S_r;
        Q_target = 0;
    } else if (_P_target < -S_r) {
        P_target = -S_r;
        Q_target = 0;
    } else {
        P_target = _P_target;

        double q1, q2;
        double q_available;
        q1 = sqrt(S_r * S_r - P_target * P_target);
        q2 = fabs(P_target * tan(acos(pf_min)));
        if (q1 < q2) {
            q_available = q1;
        } else {
            q_available = q2;
        }
        if (fabs(_Q_target) > q_available) {
            Q_target = (_Q_target/fabs(_Q_target)) * q_available;
        } else {
            Q_target = _Q_target;
        }
    }
    _P_target = P_target;
    return;
}