/**
 * This file belongs to DistAIX FBScomponents
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/

#include "model/ideal_transformer.h"

Ideal_transformer::Ideal_transformer(double _stepsize, double _v_nom, double _S_r, double _ratio_nom)
    : Two_port_grounded(_stepsize, _v_nom, _S_r), ratio_nom(_ratio_nom)
{
}

/*!
 * \brief proceed to next time step
 * \param t next time step
 */
void Ideal_transformer::step(double t)
{
    t_prev = t_next;
    t_next = t;
    if(first_step) {
        first_step = false;
    }
    return;
}

void Ideal_transformer::set_ratio_mul(double ratio_mul)
{
    ratio = ratio_nom * ratio_mul;
    return;
}

/*!
 * \brief solve model
 * \param _v1_re [in]   input voltage
 * \param _v1_im [in]   input voltage
 * \param _i1_re [in]   input current
 * \param _i1_im [in]   input current
 * \param _v2_re [out]  output voltage
 * \param _v2_im [out]  output voltage
 * \param _i2_re [out]  output current
 * \param _i2_im [out]  output current
 */
void Ideal_transformer::solve(double _v1_re, double _v1_im, double _i1_re, double _i1_im, double &_v2_re,
    double &_v2_im, double &_i2_re, double &_i2_im)
{
    v1_re = _v1_re;
    v1_im = _v1_im;
    i1_re = _i1_re;
    i1_im = _i1_im;

    /* output voltage of transformer v2 = v_rx / (ratio * ratio_mul) */
    v2_re = v1_re / ratio;
    v2_im = v1_im / ratio;
    /* output current of transformer: i2 = -i_rx * (ratio * ratio) */
    i2_re = -i1_re * ratio;
    i2_im = -i1_im * ratio;

    _v2_re = v2_re;
    _v2_im = v2_im;
    _i2_re = i2_re;
    _i2_im = i2_im;
    return;
}