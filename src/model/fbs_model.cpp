/**
 * This file belongs to DistAIX FBScomponents
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/

#include "model/fbs_model.h"
#include "fbs_config.h"

/*!
 * \brief Construct a new fbs model::fbs model object
 * \param _v_nom nominal voltage
 */
FBS_model::FBS_model(double _stepsize, double _v_nom, double _S_r) : stepsize(_stepsize),
    v_nom(_v_nom), S_r(_S_r)
{
    t_next = 0;
    f_nom = 50.0;
    omega_nom = f_nom*2*3.14159;  /* Omega = 2 * pi * fnom*/
    first_step = true;
}