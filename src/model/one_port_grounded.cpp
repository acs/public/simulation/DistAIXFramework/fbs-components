/**
 * This file belongs to DistAIX FBScomponents
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/

#include "model/one_port_grounded.h"

/*!
 * \brief Construct a new One_port::One_port object
 * \param _v_nom nominal voltage
 */
One_port_grounded::One_port_grounded(double _stepsize, double _v_nom, double _S_r) 
    : FBS_model(_stepsize, _v_nom, _S_r)
{
    v_re = _v_nom;
    v_im = 0;
    i_re = 0;
    i_im = 0;
    P = 0;
    Q = 0;
}

void One_port_grounded::calculate_power()
{
    P = 3 * (v_re * i_re + v_im * i_im);
    Q = 3 * (v_im * i_re - v_re * i_im);
}

/*!
 * \brief get power of one port
 * \param _P [out] active power
 * \param _Q [out] reactive power
 */
void One_port_grounded::get_power(double &_P, double &_Q)
{
    _P = P;
    _Q = Q;
    return;
}

/*!
 * \brief get current
 * \param _i_re real part of current
 * \param _i_im imaginary part of current
 */
void One_port_grounded::get_current(double &_i_re, double &_i_im)
{
    _i_re = i_re;
    _i_im = i_im;
    return;
}