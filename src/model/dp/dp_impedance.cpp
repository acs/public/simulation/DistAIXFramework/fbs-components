/**
 * This file belongs to DistAIX FBScomponents
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/

#include "model/dp/dp_impedance.h"
#include "fbs_config.h"

/*!
 * \brief Construct a new DP_impedance:: DP_impedance object
 * \param _v_nom    nominal voltage
 * \param _S_r      rated power
 * \param _pf_min   minimum power factor
 */
DP_impedance::DP_impedance(double _stepsize, double _v_nom, double _S_r)
    : Impedance(_stepsize, _v_nom, _S_r)
{
}

/*!
 * \brief proceed to next time step
 * \param t next time step
 */
void DP_impedance::step(double t)
{
    t_prev = t_next;
    t_next = t;
    if(first_step) {
        first_step = false;
    }
    i_prev_re = i_re;
    i_prev_im = i_im;
    return;
}

/*!
 * \brief initalize state with steady state
 */
void DP_impedance::init()
{
    double Z_square = Z_re * Z_re + Z_im * Z_im;
    i_re = (v_re * Z_re + v_im * Z_im) / Z_square;
    i_im = (v_im * Z_re - v_re * Z_im) / Z_square;
    return;
}

/*!
 * \brief calculate current for given voltage
 * \param _v_re [in] real part of voltage
 * \param _v_im [in] imaginary part of voltage
 * \param _i_re [out] real part of current
 * \param _i_im [out] imaginary part of current
 */
void DP_impedance::solve(double _v_re, double _v_im, double &_i_re, double &_i_im)
{
    v_re = _v_re;
    v_im = _v_im;

    if (first_step){
        init();
    } else {
        double Z_dyn = Z_im / (omega_nom * stepsize);
        double v_re_temp = v_re + Z_dyn * i_prev_re;
        double v_im_temp = v_im + Z_dyn * i_prev_im;
        double Z_re_temp = Z_re + Z_dyn;
        double Z_square = Z_re_temp * Z_re_temp + Z_im * Z_im;
        i_re = (v_re_temp * Z_re_temp + v_im_temp * Z_im) / Z_square;
        i_im = (v_im_temp * Z_re_temp - v_re_temp * Z_im) / Z_square;
    }

    _i_re = i_re;
    _i_im = i_im;
    return;
}