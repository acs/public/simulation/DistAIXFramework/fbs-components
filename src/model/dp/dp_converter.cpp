/**
 * This file belongs to DistAIX FBScomponents
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/

#include "model/dp/dp_converter.h"
#include "fbs_config.h"
#include <cstring>
#include <cmath>

/*!
 * \brief Construct a new DP_converter:: DP_converter object
 * \param _v_nom    nominal voltage
 * \param _S_r      rated power
 * \param _pf_min   minimum power factor
 */
DP_converter::DP_converter(double _stepsize, double _v_nom, double _S_r, double _pf_min)
    : Converter(_stepsize, _v_nom, _S_r, _pf_min)
{
    double _pm[N_EQN*N_EQN] = {
            1, 0, 0, 0, 0, 0,
            0, 1, 0, 0, 0, 0,
            0, 0, 1, 0, 0, 0,
            0, 0, 0, 1, 0, 0,
            0, 0, 0, 0, 1, 0,
            0, 0, 0, 0, 0, 1};
    std::memcpy(pm, _pm, sizeof(pm));
    stepsize = _stepsize;
    set_A();
}

/*!
 * \brief proceed to next time step
 * \param t next time step
 */
void DP_converter::step(double t)
{
    t_prev = t_next;
    t_next = t;
    if(first_step) {
        first_step = false;
    }

    /* cpoy temporary results to final state */
    std::memcpy(x_re, x_temp_re, sizeof(x_re));
    std::memcpy(x_im, x_temp_im, sizeof(x_im));
    return;
}

/*!
 * \brief initalize state with steady state
 */
void DP_converter::init()
{
    double v_square = v_re * v_re + v_im * v_im; // <- v: V_1
    x_temp_re[3] = (P_target * v_re + Q_target * v_im) / (3 * v_square); // <- x[3]: I_1 = I_s
    x_temp_im[3] = (P_target * v_im - Q_target * v_re) / (3 * v_square);

    // V_C = V_1 - jwL_1*I_1
    x_temp_re[2] = v_re + omega_nom * L_1 * x_temp_im[3]; // <- x[2]: V_C
    x_temp_im[2] = v_im - omega_nom * L_1 * x_temp_re[3];

    // I_2 = -I_1 + jwC*V_C
    x_temp_re[1] = - x_temp_re[3] - omega_nom * C * x_temp_im[2]; // <- x[1]: I_2
    x_temp_im[1] = - x_temp_im[3] + omega_nom * C * x_temp_re[2];

    // V_2 = V_C + jwL_2*I_2
    x_temp_re[0] = x_temp_re[2] - omega_nom * L_2 * x_temp_im[1]; // <- x[0]: V_2
    x_temp_im[0] = x_temp_im[2] + omega_nom * L_2 * x_temp_re[1];

    x_temp_re[4] = 0; // <- x[4]: X_1
    x_temp_im[4] = 0;

    x_temp_re[5] = x_temp_re[0] / K_r; // <- x[5]: X_2
    x_temp_im[5] = x_temp_im[0] / K_r;
}

/*!
 * \brief calculate current for given voltage
 * \param _v_re [in] real part of voltage
 * \param _v_im [in] imaginary part of voltage
 * \param _i_re [out] real part of current
 * \param _i_im [out] imaginary part of current
 */
void DP_converter::solve(double _v_re, double _v_im, double &_i_re, double &_i_im)
{
    v_re = _v_re;
    v_im = _v_im;

    if (first_step){
        init();
    } else {
        /* set result vector */
        set_B();

        /* solve Ax=B */
        // x[0]: V_2, x[1]: I_2, x[2]: V_C,
        // x[3]: I_1, x[4]: X_1, x[5]: X_2

        //initialize x and y to the same size as b
        double y_re [N_EQN];
        double y_im [N_EQN];
        for(int i = 0;i < N_EQN; i++){
            y_re[i] = 0;
            y_im[i] = 0;
        }

        for (int i = 0; i < N_EQN; i++) {
            /* forward substitution */
            for (int j = 0; j < N_EQN; j++) { // partial pivote b with Permutation Matrix P
                y_re[i] += pm[i*N_EQN + j] * B_re[j];
                y_im[i] += pm[i*N_EQN + j] * B_im[j];
            }
            for (int j = 0; j < i; j++) {
                /* y[i] -= LU[i][j]*y[j] */
                double tmp = y_re[j];
                y_re[i] -=  (A_re[i*N_EQN + j] * y_re[j] - A_im[i*N_EQN + j] * y_im[j]);
                y_im[i] -=  (A_re[i*N_EQN + j] * y_im[j] + A_im[i*N_EQN + j] * tmp);
            }
        }
        for (int i = N_EQN-1; i > -1; i--) {
            /* back substitution */
            x_temp_re[i] = y_re[i];
            x_temp_im[i] = y_im[i];
            for (int j = i+1; j < N_EQN; j++){
                /* x[i] -= LU[i][j]*x[j] */
                double tmp = x_temp_re[j];
                x_temp_re[i] -= (A_re[i*N_EQN + j] * x_temp_re[j] - A_im[i*N_EQN + j] 
                    * x_temp_im[j]);
                x_temp_im[i] -= (A_re[i*N_EQN + j] * x_temp_im[j] + A_im[i*N_EQN + j] * tmp);
            }
            /* x[i] = x[i]/LU[i][i] */
            // <- calculate just ones and save to cache
            double cache = pow(A_re[i*N_EQN + i],2) + pow(A_im[i*N_EQN + i],2);
            double tmp = x_temp_re[i];
            x_temp_re[i] = (x_temp_re[i] * A_re[i*N_EQN + i] + x_temp_im[i] * A_im[i*N_EQN + i])
                / cache;
            x_temp_im[i] = (x_temp_im[i] * A_re[i*N_EQN + i] - tmp * A_im[i*N_EQN + i])/cache;
        }
    }

    i_re = x_temp_re[3];
    i_im = x_temp_im[3];
    _i_re = i_re;
    _i_im = i_im;
    return;
}

/*! \brief set real and imaginary part of matrix A in linear equation system of converter
*/
void DP_converter::set_A() {
    double A_temp_re[] = {
            1., 0, 0, -K_p, -K_r, -K_r,
            -1./L_2, 1./stepsize, 1./L_2, 0, 0, 0,
            0, -1./C, 1./stepsize, -1./C, 0, 0,
            0, 0, 1./L_1, 1./stepsize, 0, 0,
            0, 0, 0, -1./2., 1./stepsize, 0,
            0, 0, 0, -1./2., 0, 1./stepsize
    };
    double A_temp_im[] = {
            0, 0, 0, 0, 0, 0,
            0, omega_nom, 0, 0, 0, 0,
            0, 0, omega_nom, 0, 0, 0,
            0, 0, 0, omega_nom, 0, 0,
            0, 0, 0, 0, 2*omega_nom, 0,
            0, 0, 0, 0, 0, 0
    };
    memcpy(A_re, A_temp_re, sizeof(A_re));
    memcpy(A_im, A_temp_im, sizeof(A_im));

    partial_pivoting();
    LU_decomposition();
}

/*! \brief set real and imaginary part of vector B in linear equation system of converter
 * \param v_re [in]     Real part of the voltage
 * \param v_im [in]     Imaginary part of the voltage
*/
void DP_converter::set_B() {
    double v_square = v_re * v_re + v_im * v_im; // <- v:
    double i_s_re = (P_target * v_re + Q_target * v_im) / (3 * v_square);
    double i_s_im = (P_target * v_im - Q_target * v_re) / (3 * v_square);
    double b_re [] = { // Real part of resultant vector AX = B
            -K_p * i_s_re,
            x_re[1] / stepsize, // <- x[1]: I_2
            x_re[2] / stepsize, // <- x[2]: V_C
            x_re[3] / stepsize + v_re / L_1, // <- v: V_1, x[3]: I_1
            x_re[4] / stepsize - i_s_re / 2., // <- x[4]: X_1
            x_re[5] / stepsize - i_s_re / 2. // <- x[5]: X_2
    };
    double b_im [] = { // Imaginary part of resultant vector AX = B
            -K_p * i_s_im,
            x_im[1] /stepsize, // <- x[1]: I_2
            x_im[2] / stepsize, // <- x[2]: V_C
            x_im[3] / stepsize + v_im / L_1, // <- v: V_1, x[3]: I_1
            x_im[4] / stepsize - i_s_im / 2., // <- x[4]: X_1
            x_im[5] / stepsize - i_s_im / 2. // <- x[5]: X_2
    };

    memcpy(B_re, b_re, sizeof(b_re));// save in Datastruct (not necessary)
    memcpy(B_im, b_im, sizeof(b_im));
}

/*! \brief LU Factorization
*/
void DP_converter::LU_decomposition() {
    /* Set up the linear equation system
    if values change over time this has to be done every step */
    
    /* Overwrite A Matrix with LU Matrix */
    for (int i = 0; i < N_EQN; i++) {
        for (int j = i; j < N_EQN; j++) { // Determine U across row i
            for (int k = 0; k < i; k++) {
                double tmp = A_re[k*N_EQN + j]; // <- beware of overwriting while calculation
                // A[i][j] -= A[i][k]*A[k][j]
                A_re[i*N_EQN + j] -= (A_re[i*N_EQN + k]*A_re[k*N_EQN + j] 
                    - A_im[i*N_EQN + k]*A_im[k*N_EQN + j]);
                A_im[i*N_EQN + j] -= (A_re[i*N_EQN + k]*A_im[k*N_EQN + j] 
                    + A_im[i*N_EQN + k]*tmp);
            }
        }
        for (int j = i + 1; j < N_EQN; j++) { // Determine L down column i
            for (int k = 0; k < i; k++) {
                double tmp = A_re[j*N_EQN + k]; // <- beware of overwriting while calculation
                // A[j][i] -= A[j][k]*A[k][i]
                A_re[j*N_EQN + i] -= (A_re[j*N_EQN + k]*A_re[k*N_EQN + i] 
                    - A_im[j*N_EQN + k]*A_im[k*N_EQN + i]);
                A_im[j*N_EQN + i] -= (tmp*A_im[k*N_EQN + i] + A_im[j*N_EQN + k]*A_re[k*N_EQN + i]);
            }
            // <- calculate just ones and save to cache
            double cache = pow(A_re[i*N_EQN + i],2)+pow(A_im[i*N_EQN + i],2);
            double tmp = A_re[j*N_EQN + i]; // <- beware of overwriting while calculation
            // A[j][i] = A[j][i]/A[i][i]
            A_re[j*N_EQN + i] = (A_re[j*N_EQN + i]*A_re[i*N_EQN + i] 
                + A_im[j*N_EQN + i]*A_im[i*N_EQN + i])/cache;
            A_im[j*N_EQN + i] = (A_im[j*N_EQN + i]*A_re[i*N_EQN + i] - tmp*A_im[i*N_EQN + i])/cache;
        }
    }
}

/*! \brief Partial Pivoting increases stability
*/
void DP_converter::partial_pivoting() {

    memset(pm,0,N_EQN*N_EQN*sizeof(double));
    for (int i = 0; i < N_EQN; i++) {
        pm[i*N_EQN + i] = 1.;
    }

    int row = 0;
    for (int i = 0; i < N_EQN; i++) {
        double Umax = 0;
        for (int j = i; j < N_EQN; j++) {
            double Uii = A_re[j*N_EQN + i];
            for (int k = 0; k < i; k++) {
                Uii -= A_re[j*N_EQN + k]*A_re[k*N_EQN + j];
            }
            if (fabs(Uii)>Umax) {
                Umax = fabs(Uii);
                row = j;
            }
        }
        if (i != row) {//swap rows
            for (int k = 0; k < N_EQN; k++) {
                double tmp = pm[i*N_EQN + k];  // <- pivoting Matrix
                pm[i*N_EQN + k] = pm[row*N_EQN + k];
                pm[row*N_EQN + k] = tmp;

                tmp = A_re[i*N_EQN + k]; // <- swap imaginary
                A_re[i*N_EQN + k] = A_re[row*N_EQN + k];
                A_re[row*N_EQN + k] = tmp;
                tmp = A_im[i*N_EQN + k]; // <- swap imaginary
                A_im[i*N_EQN + k] = A_im[row*N_EQN + k];
                A_im[row*N_EQN + k] = tmp;
            }
        }
    }
}
