/**
 * This file belongs to DistAIX FBScomponents
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/

#include "model/dp/dp_line.h"
#include "fbs_config.h"
#include "cstring"

DP_Line::DP_Line(double _stepsize, double _v_nom, double _S_r, double _R, double _X, double _G,
        double _B)
    : Line(_stepsize, _v_nom, _S_r, _R, _X, _G, _B)
{
}

/*!
 * \brief proceed to next time step
 * \param t next time step
 */
void DP_Line::step(double t)
{
    t_prev = t_next;
    t_next = t;
    if(first_step) {
        first_step = false;
    }
    /* copy temporary results to final state */
    v1_prev_re = v1_re;
    v1_prev_im = v1_im;
    v2_prev_re = v2_re;
    v2_prev_im = v2_im;
    i_rx_prev_re = i_rx_re;
    i_rx_prev_im = i_rx_im;
    return;
}

/*!
 * \brief initialize state
 */
void DP_Line::init()
{
    /* leakage current at input terminal: i1_leak = v1*(G + jB)/2 */
    i1_leak_re = (v1_re * G - v1_im * B) / 2;
    i1_leak_im = (v1_im * G + v1_re * B) / 2;
    /* current through RX segment: i_rx = i1 - i1_leak */
    double i_rx_re = i1_re - i1_leak_re;
    double i_rx_im = i1_im - i1_leak_im;

    /* output voltage: v2 = v1 - i_rx*(r + jX) */
    v2_re = v1_re - (i_rx_re * R - i_rx_im * X);
    v2_im = v1_im - (i_rx_re * X + i_rx_im * R);

    /* leakage current at output terminal: i2_leak = v2*(G + jB)/2 */
    i2_leak_re = (v2_re * G - v2_im * B) / 2;
    i2_leak_im = (v2_im * G + v2_re * B) / 2;
    /* output current: i2 = - i_rx + i2_leak */
    i2_re = - i_rx_re + i2_leak_re;
    i2_im = - i_rx_im + i2_leak_im;
}

/*!
 * \brief solve pi line model
 * \param _v1_re [in]   input voltage
 * \param _v1_im [in]   input voltage
 * \param _i1_re [in]   input current
 * \param _i1_im [in]   input current
 * \param _v2_re [out]  output voltage
 * \param _v2_im [out]  output voltage
 * \param _i2_re [out]  output current
 * \param _i2_im [out]  output current
 */
void DP_Line::solve(double _v1_re, double _v1_im, double _i1_re, double _i1_im, double &_v2_re,
    double &_v2_im, double &_i2_re, double &_i2_im)
{
    v1_re = _v1_re;
    v1_im = _v1_im;
    i1_re = _i1_re;
    i1_im = _i1_im;

    if (first_step){
        init();
    } else {
        /* variables for convenience */
        double a = B / (omega_nom * stepsize);
        double b = X / (omega_nom * stepsize);

        /* calculate output voltage and current */
        /* leakage current at input terminal: i1_leak = v1*(G + jB)/2 - v1_prev*X_C/(2*omega*t_tep) */
        i1_leak_re = v1_re * (a + G) / 2 - v1_prev_re * a / 2 - v1_im * B / 2;
        i1_leak_im = v1_im * (a + G) / 2 - v1_prev_im * a / 2 + v1_re * B / 2;

        /* current through RX segment: i_rx = i1 - i1_leak */
        i_rx_re = i1_re - i1_leak_re;
        i_rx_im = i1_im - i1_leak_im;

        /* output voltage: v2 = v1 - (i_rx*(r + jX_L) - i_rx_prev*X_L/(omega*t_tep)) */
        v2_re = v1_re - (i_rx_re * (b + R) - i_rx_prev_re * b - i_rx_im * X);
        v2_im = v1_im - (i_rx_im * (b + R) - i_rx_prev_im * b + i_rx_re * X);

        /* leakage current at output terminal: i2_leak = v2*(G + jB)/2 - v2_prev*X_C/(2*omega*t_tep) */
        i2_leak_re = v2_re * (a + G) / 2 - v2_prev_re * a / 2 - v2_im * B / 2;
        i2_leak_im = v2_im * (a + G) / 2 - v2_prev_im * a / 2 + v2_re * B / 2;

        /* output current: i2 = - i_rx + i2_leak */
        i2_re = - i_rx_re + i2_leak_re;
        i2_im = - i_rx_im + i2_leak_im;
    }

    _v2_re = v2_re;
    _v2_im = v2_im;
    _i2_re = i2_re;
    _i2_im = i2_im;
    return;
}