/**
 * This file belongs to DistAIX FBScomponents
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/

#include "model/sp/sp_converter.h"

/*!
 * \brief Construct a new SP_converter:: SP_converter object
 * \param _v_nom    nominal voltage
 * \param _S_r      rated power
 * \param _pf_min   minimum power factor
 */
SP_converter::SP_converter(double _stepsize, double _v_nom, double _S_r, double _pf_min)
    : Converter(_stepsize, _v_nom, _S_r, _pf_min)
{
}

/*!
 * \brief proceed to next time step
 * \param t next time step
 */
void SP_converter::step(double t)
{
    t_prev = t_next;
    t_next = t;
    if(first_step) {
        first_step = false;
    }
    return;
}

/*!
 * \brief calculate current for given voltage
 * \param _v_re [in] real part of voltage
 * \param _v_im [in] imaginary part of voltage
 * \param _i_re [out] real part of current
 * \param _i_im [out] imaginary part of current
 */
void SP_converter::solve(double _v_re, double _v_im, double &_i_re, double &_i_im)
{
    v_re = _v_re;
    v_im = _v_im;

    double v_square = v_re * v_re + v_im * v_im;
    i_re = (P_target * v_re + Q_target * v_im) / (3 * v_square);
    i_im = (P_target * v_im - Q_target * v_re) / (3 * v_square);

    _i_re = i_re;
    _i_im = i_im;
    return;
}