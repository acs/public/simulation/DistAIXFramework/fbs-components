/**
 * This file belongs to DistAIX FBScomponents
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/

#include "model/impedance.h"

/*!
 * \brief Construct a new Converter:: Converter object
 * \param _v_nom    nominal voltage
 * \param _S_r      rated power
 * \param _pf_min   minimum power factor
 */
Impedance::Impedance(double _stepsize, double _v_nom, double _S_r)
    : One_port_grounded(_stepsize, _v_nom, _S_r)
{
}

/*!
 * \brief set the target power for converter control
 * \param _R real part of impedance
 * \param _XL inductive reactance
 * \param _XC capacitive reactance
 */
void Impedance::set_impedance_value(double _Z_re, double _Z_im)
{
    Z_re = _Z_re;
    Z_im = _Z_im;
    return;
}