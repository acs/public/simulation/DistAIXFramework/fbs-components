/**
 * This file belongs to DistAIX FBScomponents
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/

#include "model/two_port_grounded.h"

/*!
 * \brief Construct a new Two_port_grounded::Two_port_grounded object
 * \param _v_nom nominal voltage
 * \param _S_r rated power
 */
Two_port_grounded::Two_port_grounded(double _stepsize, double _v_nom, double _S_r)
    : FBS_model(_stepsize, _v_nom, _S_r)
{
    v1_re = _v_nom;
    v1_im = 0;
    i1_re = 0;
    i1_im = 0;
    P1 = 0;
    Q1 = 0;
    v2_re = _v_nom;
    v2_im = 0;
    i2_re = 0;
    i2_im = 0;
    P2 = 0;
    Q2 = 0;
}

/*!
 * \brief calculate power input and output
 */
void Two_port_grounded::calculate_power()
{
    P1 = 3 * (v1_re * i1_re + v1_im * i1_im);
    Q1 = 3 * (v1_im * i1_re - v1_re * i1_im);
    P2 = 3 * (v2_re * i2_re + v2_im * i2_im);
    Q2 = 3 * (v2_im * i2_re - v2_re * i2_im);
    return;
}

/*!
 * \brief get power of one port
 * \param _P1 [out] active power input
 * \param _Q1 [out] reactive power input
 * \param _P1 [out] active power output
 * \param _Q1 [out] reactive power output
 */
void Two_port_grounded::get_power(double &_P1, double &_Q1, double &_P2, double &_Q2)
{
    _P1 = P1;
    _Q1 = Q1;
    _P2 = P2;
    _Q2 = Q2;
    return;
}
