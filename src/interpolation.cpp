/**
 * This file belongs to DistAIX FBScomponents
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/

#include <iostream>
#include "interpolation.h"
#include "fbs_config.h"

/*! \brief Constructor of Interpolation class
 * \param type [in] 			type of the interpolation (can be linear or hold)
 * \param time_values [in] 		values of time axis
 * \param data [in] 			values of data axis
 * \param number_values [in] 	number of samples in time_values and data
 * */
Interpolation::Interpolation(std::string type, double *t, double *v, int _number_values, std::string &_header)
    : data(v), time_values(t), interpolation_type(type)
{
    number_values = (unsigned int) _number_values;
    if(interpolation_type=="linear"){
        profile_interpolated = gsl_interp_alloc (gsl_interp_linear,number_values);
        gsl_interp_init(profile_interpolated, t, v, number_values );
        accelerator =  gsl_interp_accel_alloc();
    }
    else if(interpolation_type == "hold"){
        current_position = 0;
    }
    else {
        std::cerr<< "Warning: Unknown interpolation type: "<< interpolation_type <<". Use hold as default."
            << std::endl;
        interpolation_type = "hold";
        current_position = 0;
    }

    max_time_value = time_values[number_values-1];
}

/*! \brief obtain the interpolated value for a given time
 * \param _time_step [in] 	time for which the interpolated value shall be determined
 * \return 					interpolated value
 * */
double Interpolation::get_interpolated_value(double _time_step){
    double ret_value = 0.0;
    //bool reset = false;

    double current_step = _time_step;
    if (_time_step + EPSILON  > max_time_value) {
        //periodic extrapolation
        unsigned int n = _time_step / max_time_value; //integer result is intended!
        current_step = _time_step - n * max_time_value;
    }


    //interpolate linear using gsl
    if(interpolation_type == "linear"){
        ret_value = gsl_interp_eval(profile_interpolated, (double*) &(time_values[0]), (double*) &(data[0]),
            current_step, accelerator);
    }
    //hold value until next timestep
    else if(interpolation_type == "hold"){
        while(true){
            unsigned int next_position = current_position +1;
            if(next_position > (number_values-2))
                next_position -= (number_values-1);
            if(current_step +EPSILON>= time_values[current_position] && current_step < time_values[next_position]){
                ret_value = data[current_position];
                break;
            }
            else if(next_position == 0 && current_step + EPSILON >= time_values[number_values-2])
            {	
                ret_value = data[current_position];
                break;
            }
            else{
                current_position ++;
            
                if(current_position > (number_values-2)) {
                    current_position -= (number_values - 1);
                }
            
            }
        }
    
    }
    else{
        std::cerr << "ERROR: Invalid interpolation type: " << interpolation_type << std::endl;
    }

    return ret_value;
}

double Interpolation::get_max_time()
{
    return max_time_value;
}