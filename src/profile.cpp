/**
 * This file belongs to DistAIX FBScomponents
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/

#include "profile.h"

Profile::Profile()
{

}

void Profile::init(std::string interp_type)
{
    interpolation_type = interp_type;
}

Profile::~Profile() {
    // delete Interpolation elements
    load_P_profiles.clear();
    load_Q_profiles.clear();
    pv_P_profiles.clear();
    wec_P_profiles.clear();
    th_profiles.clear();
    ev_conn_profiles.clear();
    ev_P_profiles.clear();
    bio_P_profiles.clear();
}

void Profile::add_profile(int profile_type, double* t, double *v, unsigned long int number_of_values,
        std::string header)
{
    //initialize interpolated load profile
        Interpolation* interpolated_profile;
    if (profile_type == PROFILE_EV_CONN) {
        interpolated_profile = new Interpolation("hold", t, v, number_of_values, header);
    } else {
        interpolated_profile = new Interpolation(interpolation_type, t, v, number_of_values, header);
    }

    switch (profile_type) {
        case PROFILE_LOAD_P: {
            load_P_profiles.push_back(interpolated_profile);
            break;
        }
        case PROFILE_LOAD_Q: {
            load_Q_profiles.push_back(interpolated_profile);
            break;
        }
        case PROFILE_PV_P: {
            pv_P_profiles.push_back(interpolated_profile);
            break;
        }
        case PROFILE_WEC_P: {
            wec_P_profiles.push_back(interpolated_profile);
            break;
        }
        case PROFILE_TH: {
            th_profiles.push_back(interpolated_profile);
            break;
        }
        case PROFILE_EV_CONN: {
            ev_conn_profiles.push_back(interpolated_profile);
            break;
        }
        case PROFILE_EV_P: {
            ev_P_profiles.push_back(interpolated_profile);
            break;
        }
        case PROFILE_BIO_P: {
            bio_P_profiles.push_back(interpolated_profile);
            break;
        }
    }

    return;
}

double Profile::get_profile_value(int profile_type, unsigned int id, double t)
{
    double value = 0;

    /* ids start at 1. Hence, index is id-1 */
    switch (profile_type) {
        case PROFILE_LOAD_P: {
            //if (id <= load_P_profiles.size()) {
                value = load_P_profiles[id-1]->get_interpolated_value(t);
            //}
            break;
        }
        case PROFILE_LOAD_Q: {
            //if (id <= load_Q_profiles.size()) {
                value = load_Q_profiles[id-1]->get_interpolated_value(t);
            //}
            break;
        }
        case PROFILE_PV_P: {
            //if (id <= pv_P_profiles.size()) {
                value = pv_P_profiles[id-1]->get_interpolated_value(t);
            //}
            break;
        }
        case PROFILE_WEC_P: {
            //if (id <= wec_P_profiles.size()) {
                value = wec_P_profiles[id-1]->get_interpolated_value(t);
            //}
            break;
        }
        case PROFILE_TH: {
            //if (id <= th_profiles.size()) {
                value = th_profiles[id-1]->get_interpolated_value(t);
            //}
            break;
        }
        case PROFILE_EV_CONN: {
            //if (id <= ev_conn_profiles.size()) {
                value = ev_conn_profiles[id-1]->get_interpolated_value(t);
            //}
            break;
        }
        case PROFILE_EV_P: {
            //if (id <= ev_P_profiles.size()) {
                value = ev_P_profiles[id-1]->get_interpolated_value(t);
            //}
            break;
        }
        case PROFILE_BIO_P: {
            //if (id <= bio_P_profiles.size()) {
                value = bio_P_profiles[id-1]->get_interpolated_value(t);
            //}
            break;
        }
    }

    return value;
}

double Profile::get_max_time(int profile_type, unsigned int id)
{
    double value = 0;

    /* ids start at 1. Hence, index is id-1 */
    switch (profile_type) {
        case PROFILE_LOAD_P: {
            //if (id <= load_P_profiles.size()) {
                value = load_P_profiles[id-1]->get_max_time();
            //}
            break;
        }
        case PROFILE_LOAD_Q: {
            //if (id <= load_Q_profiles.size()) {
                value = load_Q_profiles[id-1]->get_max_time();
            //}
            break;
        }
        case PROFILE_PV_P: {
            //if (id <= pv_P_profiles.size()) {
                value = pv_P_profiles[id-1]->get_max_time();
            //}
            break;
        }
        case PROFILE_WEC_P: {
            //if (id <= wec_P_profiles.size()) {
                value = wec_P_profiles[id-1]->get_max_time();
            //}
            break;
        }
        case PROFILE_TH: {
            //if (id <= th_profiles.size()) {
                value = th_profiles[id-1]->get_max_time();
            //}
            break;
        }
        case PROFILE_EV_CONN: {
            //if (id <= ev_conn_profiles.size()) {
                value = ev_conn_profiles[id-1]->get_max_time();
            //}
            break;
        }
        case PROFILE_EV_P: {
            //if (id <= ev_P_profiles.size()) {
                value = ev_P_profiles[id-1]->get_max_time();
            //}
            break;
        }
        case PROFILE_BIO_P: {
            //if (id <= bio_P_profiles.size()) {
                value = bio_P_profiles[id-1]->get_max_time();
            //}
            break;
        }
    }

    return value;
}


