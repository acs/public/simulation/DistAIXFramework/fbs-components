/**
 * This file belongs to DistAIX FBScomponents
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/

#include "component/load.h"
#include "fbs_config.h"
#include "model/dp/dp_converter.h"
#include "model/sp/sp_converter.h"

/*!
 * \brief Construct a new Load:: Load object
 * \param _model_type   steady state or dynamic phasor
 * \param _v_nom        nominal voltage
 * \param _S_r          rated power
 * \param _pf_min       minimum power factor
 * \param profiles      pointer to profile object
 * \param p_id          ID of active power demand profile
 * \param q_id          ID of reactive power demand profile
 * \param scale         scale factor for profile values
 */
Load::Load(int _model_type, double _stepsize, double _v_nom, double _S_r, double _pf_min,
    Profile* profiles, int p_id, int q_id, double scale) 
    :Prosumer(_model_type, _v_nom, profiles), profile_id_p(p_id), profile_id_q(q_id),
        profile_scale_factor(scale)
{
    read_next_profile_value();
    One_port_grounded *model_temp;
    if (_model_type == MODEL_TYPE_DYNAMIC_PHASOR) {
        model_temp = new DP_converter(_stepsize, _v_nom, _S_r, _pf_min);
    } else {
        /* default is static phasor */
        model_temp = new SP_converter(_stepsize, _v_nom, _S_r, _pf_min);
    }
    std::pair<int, One_port_grounded*> converter_temp(TYPE_OPG_CONVERTER, model_temp);
    models.push_back(converter_temp);
}

/*!
 * \brief read next values from profile
 */
void Load::read_next_profile_value()
{
    P_demand = profile_scale_factor * 
        component_profiles->get_profile_value(PROFILE_LOAD_P, profile_id_p, t_next);
    Q_demand = profile_scale_factor *
        component_profiles->get_profile_value(PROFILE_LOAD_Q, profile_id_q, t_next);
    return;
}

/*!
 * \brief set converter target power to power demand
 */
void Load::pre_processing()
{
    for (auto m : models) {
        if (m.first == TYPE_OPG_CONVERTER) {
            Converter *converter_temp = (Converter*) m.second;
            converter_temp->set_target_power(P_demand, Q_demand);
        }
    }
}

/*!
 * \brief calculate the power of all models
 */
void Load::post_processing()
{
    for (auto m : models) {
        m.second->calculate_power();
    }
}

/*!
 * \brief get power demand
 * \param _P_dem [out] active power demand
 * \param _Q_dem [out] reactive power demand
 */
void Load::get_demand(double &_P_dem, double &_Q_dem)
{
    _P_dem = P_demand;
    _Q_dem = Q_demand;
}
