/**
 * This file belongs to DistAIX FBScomponents
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/

#include "component/wec.h"
#include "fbs_config.h"
#include "model/dp/dp_converter.h"
#include "model/sp/sp_converter.h"

/*!
 * \brief Construct a new Load:: PV object
 * \param _model_type   steady state or dynamic phasor
 * \param _v_nom        nominal voltage
 * \param _S_r          rated power
 * \param _pf_min       minimum power factor
 * \param profiles      pointer to profile object
 * \param p_id          ID of active power generation profile
 * \param scale         scale factor for profile values
 */
WEC::WEC(int _model_type, double _stepsize, double _v_nom, double _S_r, double _pf_min,
    Profile* profiles, int p_id, double scale) 
    :Prosumer(_model_type, _v_nom, profiles), profile_id_p(p_id), profile_scale_factor(scale)
{
    read_next_profile_value();
    One_port_grounded *model_temp;
    if (_model_type == MODEL_TYPE_DYNAMIC_PHASOR) {
        model_temp = new DP_converter(_stepsize, _v_nom, _S_r, _pf_min);
    } else {
        /* default is static phasor */
        model_temp = new SP_converter(_stepsize, _v_nom, _S_r, _pf_min);
    }
    std::pair<int, One_port_grounded*> converter_temp(TYPE_OPG_CONVERTER, model_temp);
    models.push_back(converter_temp);
}

/*!
 * \brief read next values from profile
 */
void WEC::read_next_profile_value()
{
    P_gen = profile_scale_factor *
        component_profiles->get_profile_value(PROFILE_WEC_P, profile_id_p, t_next);
    return;
}

/*!
 * \brief set converter target power to power demand
 */
void WEC::pre_processing()
{
    if (P_ctrl > 0) {
        P_ctrl = 0;
    } else if (P_ctrl < -P_gen) {
        P_ctrl = -P_gen;
    }
    
    for (auto m : models) {
        if (m.first == TYPE_OPG_CONVERTER) {
            Converter *converter_temp = (Converter*) m.second;
            converter_temp->set_target_power(P_ctrl, Q_ctrl);
        }
    }
}

/*!
 * \brief calculate the power of all models
 */
void WEC::post_processing()
{
    for (auto m : models) {
        m.second->calculate_power();
    }
}

/*!
 * \brief get power generation
 * \return active power generation
 */
double WEC::get_generation()
{
    return P_gen;
}