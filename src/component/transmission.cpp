/**
 * This file belongs to DistAIX FBScomponents
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/

#include "component/transmission.h"
#include "fbs_config.h"
#include "model/dp/dp_line.h"
#include "model/sp/sp_line.h"

Transmission::Transmission(int _model_type, double _stepsize, double _v_nom, double _S_r, double _R,
    double _X, double _G, double _B) : FBS_component(_model_type, _v_nom)
{
    if (_model_type == MODEL_TYPE_DYNAMIC_PHASOR) {
        line = new DP_Line(_stepsize, _v_nom, _S_r, _R, _X, _G, _B);
    } else {
        /* default is static phasor */
        line = new SP_Line(_stepsize, _v_nom, _S_r, _R, _X, _G, _B);
    }
}

Transmission::~Transmission() {
    delete line;
}

/*!
 * \brief nothing to do
 */
void Transmission::pre_processing()
{
    /* nothing to do */
    return;
}

/*!
 * \brief solve line model
 * \param _v1_re [in]   input voltage
 * \param _v1_im [in]   input voltage
 * \param _i1_re [in]   input current
 * \param _i1_im [in]   input current
 * \param _v2_re [out]  output voltage
 * \param _v2_im [out]  output voltage
 * \param _i2_re [out]  output current
 * \param _i2_im [out]  output current
 */
void Transmission::solve(double _v1_re, double _v1_im, double _i1_re, double _i1_im, double &_v2_re,
    double &_v2_im, double &_i2_re, double &_i2_im)
{
    line->solve(_v1_re, _v1_im, _i1_re, _i1_im, _v2_re, _v2_im, _i2_re, _i2_im);
    return;
}

/*!
 * \brief calculate the input and output power of line
 */
void Transmission::post_processing()
{
    line->calculate_power();
    return;
}

/*!
 * \brief advance simulation for one time step
 * \param t next time step
 */
void Transmission::step(double t)
{
    t_next = t;
    line->step(t);
    return;
}

/*!
 * \brief get power
 * \param _P1 [out] active power input
 * \param _Q1 [out] reactive power input
 * \param _P1 [out] active power output
 * \param _Q1 [out] reactive power output
 */
void Transmission::get_power(double &_P1, double &_Q1, double &_P2, double &_Q2)
{
    line->get_power(_P1, _Q1, _P2, _Q2);
    return;
}

/*!
 * \brief get sum of leakage currents
 * \param _i_leak_re [out] real part of leakage current in A
 * \param _i_leak_im [out] imaginary part of leakage current in A
 */
void Transmission::get_leackage_current(double &_i_leak_re, double &_i_leak_im)
{
    line->get_leakage_current(_i_leak_re, _i_leak_im);
    return;
}


