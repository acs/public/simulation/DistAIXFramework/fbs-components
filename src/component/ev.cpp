/**
 * This file belongs to DistAIX FBScomponents
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/

#include "component/ev.h"
#include "fbs_config.h"
#include "model/dp/dp_converter.h"
#include "model/sp/sp_converter.h"
#include <cmath>

/*!
 * \brief Construct a new EV:: EV object
 * \param _model_type   steady state or dynamic phasor
 * \param _v_nom        nominal voltage
 * \param _S_r          rated power
 * \param _pf_min       minimum power factor
 * \param profiles      pointer to profile object
 * \param p_id          ID of active power generation profile
 * \param scale         scale factor for profile values
 */
EV::EV(int _model_type, double _stepsize, double _v_nom, double _S_r, double _pf_min, double _C,
        double _P_max, double _soc_init, Profile* profiles, int conn_id, int p_id, double scale) 
    :Prosumer(_model_type, _v_nom, profiles), profile_id_conn(conn_id), profile_id_p(p_id),
        profile_scale_factor(scale)
{
    connected = false;
    read_next_profile_value();
    One_port_grounded *model_temp;
    if (_model_type == MODEL_TYPE_DYNAMIC_PHASOR) {
        model_temp = new DP_converter(_stepsize, _v_nom, _S_r, _pf_min);
    } else {
        /* default is static phasor */
        model_temp = new SP_converter(_stepsize, _v_nom, _S_r, _pf_min);
    }
    std::pair<int, One_port_grounded*> converter_temp(TYPE_OPG_CONVERTER, model_temp);
    models.push_back(converter_temp);
    if(profile_id_conn != -1){ //flexible EV
	storage = new Storage(_stepsize, _C, _P_max, scale, _soc_init);
    }
    else{
	storage = nullptr;
    }
}

EV::~EV() {
    delete storage;
}

/*!
 * \brief advance simulation for one time step
 * \param t 
 */
void EV::step(double t)
{
    t_prev = t_next;
    Prosumer::step(t);
    if(storage){
        storage->step(t);
    }
    return;
}

/*!
 * \brief read next values from profile
 */
void EV::read_next_profile_value()
{   
    if(profile_id_conn != -1){ // flexible EV: two profiles are provided
	    connected_prev = connected;
	
	    P_con = profile_scale_factor *
	        component_profiles->get_profile_value(PROFILE_EV_P, profile_id_p, t_next);
	    int connected_temp =
	        nearbyint(component_profiles->get_profile_value(PROFILE_EV_CONN, profile_id_conn, t_next));
	    connected = (bool) connected_temp;
	
	    if (connected && !connected_prev) {
	        /* ev is connected and was not connected before; calculate t_conn from profile */
	        double t_temp = t_next;
	        double step_temp;
	        double t_profile_max = component_profiles->get_max_time(PROFILE_EV_CONN,
	            profile_id_conn);
	        while (connected_temp == 1) {
	            t_temp += 1;                                // increment of 1s
	            unsigned int n = t_temp / t_profile_max;    // integer results is intended!
	            step_temp = t_temp - n * t_profile_max;
	            connected_temp = nearbyint(component_profiles->get_profile_value(
	                    PROFILE_EV_CONN, profile_id_conn, step_temp));
	            //check if we have reached the end of the profile
	            if(t_temp +1 > 2*t_profile_max){
	                // if end of profile is reached, stop loop
	                // this is needed for EVs that are connected for the whole profile
	                break;
	            }
	        }
	        t_conn = t_temp - t_next;
	    }
	    else if (connected && connected_prev) {
	        /* ev is connected and was connected before; reduce t_conn by size of time step */
        	t_conn -= (t_next-t_prev);
	    }
	    else {
	        /* ev is not connected */
        	t_conn = 0;
    	}
    }
    else{ // non-flexible EV, no storage, P-profile provides P demand
        P_demand = profile_scale_factor *
                    component_profiles->get_profile_value(PROFILE_EV_P, profile_id_p, t_next);
        Q_demand = 0;
    }
}

/*!
 * \brief set converter target power
 */
void EV::pre_processing()
{
    if(storage){ //flexible EV
        if (connected) {
            double P_min, P_max;
            storage->get_power_limits(P_min, P_max);
            double E_lack = storage->get_energy_lack();
            if (E_lack < (P_max * t_conn) / 3600) {
                P_min = 0;
            } else {
                P_min = P_max; 
            }
            if (P_ctrl > P_max) {
                P_ctrl = P_max;
            } else if (P_ctrl < P_min) {
                P_ctrl = P_min;
            }
        } else {
            P_ctrl = 0;
            Q_ctrl = 0;
	}
	
        for (auto m : models) {
            if (m.first == TYPE_OPG_CONVERTER) {
                Converter *converter_temp = (Converter*) m.second;
                converter_temp->set_target_power(P_ctrl, Q_ctrl);
            }
        }
    }
    else{ //non-flexible EV
	    for (auto m : models) {
            if (m.first == TYPE_OPG_CONVERTER) {
                Converter *converter_temp = (Converter*) m.second;
                converter_temp->set_target_power(P_demand, Q_demand);
	        }
        }    
    }
}

/*!
 * \brief calculate the power of all models
 */
void EV::post_processing()
{
    if(storage){ //flexible EV
        if (connected) {
            double P_ch = 0;
            for (auto m : models) {
                double P_temp, Q_temp;
                m.second->calculate_power();
                m.second->get_power(P_temp, Q_temp);
                P_ch += P_temp;
            }
            storage->update(P_ch);
        } else {
            for (auto m : models) {
                m.second->calculate_power();
            }
            storage->update(-P_con);
        }
    }
    else{ //non-flexible EV
	    for (auto m : models) {
            m.second->calculate_power();
        }
    }
}

/*!
 * \brief get soc of storage
 * \return soc
 */
double EV::get_soc()
{
    return storage->get_soc();
}

/*!
 * \brief get energy of storage
 * \return energy
 */
double EV::get_energy()
{
    return storage->get_energy();
}

/*!
 * \brief get time until disconnection
 * \return t_conn
 */
double EV::get_t_connected()
{
    return t_conn;
}

/*!
 * \brief get connection status
 * \return connected
 */
bool EV::get_connected()
{
    return connected;
}

/*!
 * \brief get power demand of non flexible EV
 * \param _P_dem [out] active power demand
 * \param _Q_dem [out] reactive power demand
 */
void EV::get_demand(double &_P_dem, double &_Q_dem)
{
    _P_dem = P_demand;
    _Q_dem = Q_demand;
}


