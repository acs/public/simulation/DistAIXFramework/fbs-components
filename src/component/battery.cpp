/**
 * This file belongs to DistAIX FBScomponents
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/

#include "component/battery.h"
#include "fbs_config.h"
#include "model/dp/dp_converter.h"
#include "model/sp/sp_converter.h"

/*!
 * \brief Construct a new Battery:: Battery object
 * \param _model_type   steady state or dynamic phasor
 * \param _v_nom        nominal voltage
 * \param _S_r          rated power
 * \param _pf_min       minimum power factor
 * \param _C            storage capacity in Wh
 * \param _P_max        maximum charging and discharging power
 * \param _soc_init     initial soc
 */
Battery::Battery(int _model_type, double _stepsize, double _v_nom, double _S_r, double _pf_min,
    double _C, double _P_max, double _soc_init)
    :Prosumer(_model_type, _v_nom, nullptr)
{
    One_port_grounded *model_temp;
    if (_model_type == MODEL_TYPE_DYNAMIC_PHASOR) {
        model_temp = new DP_converter(_stepsize, _v_nom, _S_r, _pf_min);
    } else {
        /* default is static phasor */
        model_temp = new SP_converter(_stepsize, _v_nom, _S_r, _pf_min);
    }
    std::pair<int, One_port_grounded*> converter_temp(TYPE_OPG_CONVERTER, model_temp);
    models.push_back(converter_temp);

    storage = new Storage(_stepsize, _C, _P_max, _P_max, _soc_init);
}

Battery::~Battery() {
    delete storage;
}

/*!
 * \brief read next values from profile
 */
void Battery::read_next_profile_value()
{
    return;
}

/*!
 * \brief advance simulation for one time step
 * \param t 
 */
void Battery::step(double t)
{
    Prosumer::step(t);
    storage->step(t);
    return;
}

/*!
 * \brief set converter target power to power demand
 */
void Battery::pre_processing()
{
    double P_min, P_max;
    storage->get_power_limits(P_min, P_max);
    if (P_ctrl > P_max) {
        P_ctrl = P_max;
    } else if (P_ctrl < P_min) {
        P_ctrl = P_min;
    }
    
    for (auto m : models) {
        if (m.first == TYPE_OPG_CONVERTER) {
            Converter *converter_temp = (Converter*) m.second;
            converter_temp->set_target_power(P_ctrl, Q_ctrl);
        }
    }
    return;
}

/*!
 * \brief calculate the power of all models
 */
void Battery::post_processing()
{
    double P_ch = 0;
    for (auto m : models) {
        double P_temp, Q_temp;
        m.second->calculate_power();
        m.second->get_power(P_temp, Q_temp);
        P_ch += P_temp;
    }
    storage->update(P_ch);
    return;
}

/*!
 * \brief get soc of storage
 * \return soc
 */
double Battery::get_soc()
{
    return storage->get_soc();
}

/*!
 * \brief get energy of storage
 * \return energy
 */
double Battery::get_energy()
{
    return storage->get_energy();
}


