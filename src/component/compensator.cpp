/**
 * This file belongs to DistAIX FBScomponents
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/

#include "component/compensator.h"
#include "fbs_config.h"
#include "model/dp/dp_impedance.h"
#include "model/sp/sp_impedance.h"

/*!
 * \brief Construct a new Compensator:: Compensator object
 * \param _model_type   steady state or dynamic phasor
 * \param _v_nom        nominal voltage
 * \param _S_r          rated power
 * \param _pf_min       minimum power factor
 * \param profiles      pointer to profile object
 * \param p_id          ID of active power generation profile
 * \param scale         scale factor for profile values
 */
Compensator::Compensator(int _model_type, double _stepsize, double _v_nom, double _S_r,
    unsigned int _N) 
    :Prosumer(_model_type, _v_nom, nullptr), N(_N)
{
    One_port_grounded *model_temp;
    if (_model_type == MODEL_TYPE_DYNAMIC_PHASOR) {
        model_temp = new DP_impedance(_stepsize, _v_nom, _S_r);
    } else {
        /* default is static phasor */
        model_temp = new SP_impedance(_stepsize, _v_nom, _S_r);
    }
    std::pair<int, One_port_grounded*> impedance_temp(TYPE_OPG_IMPEDANCE, model_temp);
    models.push_back(impedance_temp);
    B_r = _S_r / (_v_nom * _v_nom);
}

/*!
 * \brief read next values from profile
 */
void Compensator::read_next_profile_value()
{
    return;
}

/*!
 * \brief set tap control value
 * \param _n_ctrl tap control value
 */
void Compensator::set_n_ctrl(unsigned int _n_ctrl)
{
    n_ctrl = _n_ctrl;
    return;
}

/*!
 * \brief set target tap
 */
void Compensator::pre_processing()
{
    if (n_ctrl < 0) {
        n_ctrl = 0;
    } else if (n_ctrl > N) {
        n_ctrl = N;
    }
    
    for (auto m : models) {
        if (m.first == TYPE_OPG_IMPEDANCE) {
            Impedance *impedance_temp = (Impedance*) m.second;
            if (n_ctrl > 0) {
                impedance_temp->set_impedance_value(0, -1.0 / (n_ctrl * B_r / N));
            } else {
                /* set impedance to high value */
                impedance_temp->set_impedance_value(1 / EPSILON, 1 / EPSILON);
            }
        }
    }
}

/*!
 * \brief calculate the power of all models
 */
void Compensator::post_processing()
{
    for (auto m : models) {
        m.second->calculate_power();
    }
}
