/**
 * This file belongs to DistAIX FBScomponents
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/

#include "component/prosumer.h"
#include <cmath>

/*!
 * \brief Construct a new Prosumer:: Prosumer object
 * \param _model_type   steady state or dynamic phasor
 * \param _v_nom        nominal voltage
 * \param _S_r          rated power
 * \param _pf_min       minimum power factor
 */
Prosumer::Prosumer(int _model_type, double _v_nom, Profile* profiles) 
    : FBS_component(_model_type, _v_nom), component_profiles(profiles)
{
}

Prosumer::~Prosumer() {
    models.clear();
}

/*!
 * \brief set power control value
 * \param _p_ctrl active power control value
 * \param _q_ctrl reactive power control value
 */
void Prosumer::set_power_ctrl(double _p_ctrl, double _q_ctrl)
{
    P_ctrl = _p_ctrl;
    Q_ctrl = _q_ctrl;
    return;
}

/*!
 * \brief get active and reactive power
 * \param _P [out] active power
 * \param _Q [out] reactive power
 */
void Prosumer::get_power(double &_P, double &_Q)
{
    _P = 0;
    _Q = 0;
    for (auto m : models) {
        double P_temp;
        double Q_temp;
        m.second->get_power(P_temp, Q_temp);
        _P += P_temp;
        _Q += Q_temp;
    }
    return;
}

/*!
 * \brief get current
 * \param _i_re [out] real part of current
 * \param _i_im [out] imaginary part of current
 */
void Prosumer::get_current(double &_i_re, double &_i_im)
{
    _i_re = 0;
    _i_im = 0;
    for (auto m : models) {
        double i_re_temp;
        double i_im_temp;
        m.second->get_current(i_re_temp, i_im_temp);
        _i_re += i_re_temp;
        _i_im += i_im_temp;
    }
    return;
}

/*!
 * \brief get power factor
 * \return power factor
 */
double Prosumer::get_pf()
{
    double P_temp, Q_temp;
    get_power(P_temp, Q_temp);
    return P_temp / sqrt(P_temp * P_temp + Q_temp * Q_temp);
}

/*!
 * \brief advance simulation for one time step
 * \param t 
 */
void Prosumer::step(double t)
{
    t_next = t;
    for (auto m : models) {
        m.second->step(t);
    }
    read_next_profile_value();
    return;
}

/*!
 * \brief calculate current for given voltage
 * \param _v_re [in] real part of voltage
 * \param _v_im [in] imaginary part of voltage
 * \param _i_re [out] real part of current
 * \param _i_im [out] imaginary part of current
 */
void Prosumer::solve(double _v_re, double _v_im, double &_i_re, double &_i_im)
{
    _i_re = 0;
    _i_im = 0;
    for (auto m : models) {
        double i_re_temp;
        double i_im_temp;
        m.second->solve(_v_re, _v_im, i_re_temp, i_im_temp);
        _i_re += i_re_temp;
        _i_im += i_im_temp;
    }
    return;
}


