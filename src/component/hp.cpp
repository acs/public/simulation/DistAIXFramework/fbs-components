/**
 * This file belongs to DistAIX FBScomponents
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/

#include "component/hp.h"
#include "fbs_config.h"
#include "model/dp/dp_converter.h"
#include "model/sp/sp_converter.h"

/*!
 * \brief Construct a new HP::HP object
 * \param _model_type   steady state or dynamic phasor
 * \param _v_nom        nominal voltage
 * \param _S_r          rated power
 * \param _pf_min       minimum power factor
 * \param _f_el         conversion between electrical and thermal power
 * \param _P_sec        nominal power of secondary heater
 * \param _C            capacity of th storage
 * \param _P_max        maximum storage charging power
 * \param _soc_init     initial th storage soc
 * \param profiles      pointer to profile object
 * \param p_id          ID of active power generation profile
 * \param scale         scale factor for profile values
 */
HP::HP(int _model_type, double _stepsize, double _v_nom, double _S_r, double _pf_min, double _P_nom, 
    double _f_el, double _P_sec, double _f_el_sec, double _C, double _P_max, double _soc_init,
    Profile* profiles, int th_id1, int th_id2, double scale, double scale_ww) 
    :Prosumer(_model_type, _v_nom, profiles), f_el(_f_el),f_el_sec(_f_el_sec), profile_id_th1(th_id1),
        profile_id_th2(th_id2), profile_scale_factor(scale), profile_scale_factor_ww(scale_ww)
{
    read_next_profile_value();
    P_max_hp = _P_nom;
    P_sec = _P_sec;
    One_port_grounded *model1_temp;
    One_port_grounded *model2_temp;
    if (_model_type == MODEL_TYPE_DYNAMIC_PHASOR) {
        model1_temp = new DP_converter(_stepsize, _v_nom, _S_r, _pf_min);
        model2_temp = new DP_converter(_stepsize, _v_nom, _P_sec, 0);
    } else {
        /* default is static phasor */
        model1_temp = new SP_converter(_stepsize, _v_nom, _S_r, _pf_min);
        model2_temp = new SP_converter(_stepsize, _v_nom, _P_sec, 0); //TODO use f_el_sec
    }
    std::pair<int, One_port_grounded*> converter_temp(TYPE_OPG_CONVERTER, model1_temp);
    std::pair<int, One_port_grounded*> sec_heater_temp(TYPE_OPG_CONVERTER, model2_temp);
    models.push_back(converter_temp);
    if (_P_sec > EPSILON) {
        models.push_back(sec_heater_temp);
    } else {
        delete model2_temp;
    }

    storage = new Storage(_stepsize, _C, _P_max, _P_max, _soc_init);
}

HP::~HP() {
    delete storage;
}

/*!
 * \brief advance simulation for one time step
 * \param t 
 */
void HP::step(double t)
{
    Prosumer::step(t);
    storage->step(t);
    return;
}

/*!
 * \brief read next values from profile
 */
void HP::read_next_profile_value()
{
    th_demand = profile_scale_factor *
        component_profiles->get_profile_value(PROFILE_TH, profile_id_th1, t_next);
    if (profile_id_th2 != -1) {
        th_demand += profile_scale_factor_ww *
        component_profiles->get_profile_value(PROFILE_TH, profile_id_th2, t_next);
    }
    return;
}

/*!
 * \brief set syngen target power to power demand
 */
void HP::pre_processing()
{
    double P_ctrl_con;
    double P_min_th, P_max_th, P_min_el, P_max_el;

    if (P_ctrl > P_max_hp) {
        P_ctrl = P_max_hp;
    }

    storage->get_power_limits(P_min_th, P_max_th);
    P_max_el = (P_max_th + th_demand) * f_el;
    P_min_el = (P_min_th + th_demand) * f_el;
    if (P_min_el < 0) {
        P_min_el = 0;
    }
    if (P_ctrl > P_max_el) {
        P_ctrl = P_max_el;
    } else if (P_ctrl < P_min_el) {
        P_ctrl = P_min_el;
    }
    if (P_ctrl > P_max_hp) {
        P_ctrl_con = P_max_hp;
        P_ctrl_sec_heater = ((P_ctrl - P_max_hp) / f_el) * f_el_sec;
        if(P_ctrl_sec_heater > P_sec){
            P_ctrl_sec_heater = P_sec;
        }
    } else {
        P_ctrl_con = P_ctrl;
        P_ctrl_sec_heater = 0;
    }
    
    for (unsigned int i = 0; i < models.size(); i++) {
        Converter *converter_temp = (Converter*) models[i].second;
        if (i == 0) {
            converter_temp->set_target_power(P_ctrl_con, Q_ctrl);
        } else if (i == 1) {
            converter_temp->set_target_power(P_ctrl_sec_heater, 0);
        }
    }
}

/*!
 * \brief calculate the power of all models
 */
void HP::post_processing()
{
    double P_ch = -th_demand;
    for (unsigned int i = 0; i < models.size(); i++) {
        double P_temp, Q_temp;
        models[i].second->calculate_power();
        models[i].second->get_power(P_temp, Q_temp);
        if (i == 0) {
            P_ch += (P_temp/f_el);
        } else if (i == 1) {
            if (f_el_sec > EPSILON) {
                P_ch += (P_temp/f_el_sec);
            }
        }
     }
    storage->update(P_ch);
    return;
}

/*!
 * \brief get soc of storage
 * \return soc
 */
double HP::get_soc()
{
    return storage->get_soc();
}

/*!
 * \brief get energy of storage
 * \return energy
 */
double HP::get_energy()
{
    return storage->get_energy();
}

/*!
 * \brief get thermal demand
 * \return thermal demand
 */
double HP::get_th_demand()
{
    return th_demand;
}

/*!
 * \brief get electrical power consumed by secondary heater
 * \return power of secondary heater
 */
double HP::get_P_sec()
{
    double _P = 0;
    double _Q = 0;
    if (models.size() > 1) {
        models[1].second->get_power(_P, _Q);
    }
    return _P;
}


