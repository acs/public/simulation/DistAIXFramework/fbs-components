/**
 * This file belongs to DistAIX FBScomponents
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/

#include "component/bio.h"
#include "fbs_config.h"
#include "model/dp/dp_syngen.h"
#include "model/sp/sp_syngen.h"

/*!
 * \brief Construct a new Bio:: Bio object
 * \param _model_type   steady state or dynamic phasor
 * \param _v_nom        nominal voltage
 * \param _S_r          rated power
 * \param _pf_min       minimum power factor
 * \param profiles      pointer to profile object
 * \param p_id          ID of active power generation profile
 * \param scale         scale factor for profile values
 */
Bio::Bio(int _model_type, double _stepsize, double _v_nom, double _S_r, double _pf_min,
    Profile* profiles, int p_id, double scale) 
    :Prosumer(_model_type, _v_nom, profiles), profile_id_p(p_id), profile_scale_factor(scale)
{
    read_next_profile_value();
    One_port_grounded *model_temp;
    if (_model_type == MODEL_TYPE_DYNAMIC_PHASOR) {
        model_temp = new DP_syngen(_stepsize, _v_nom, _S_r, _pf_min);
    } else {
        /* default is static phasor */
        model_temp = new SP_syngen(_stepsize, _v_nom, _S_r, _pf_min);
    }
    std::pair<int, One_port_grounded*> syngen_temp(TYPE_OPG_SYNGEN, model_temp);
    models.push_back(syngen_temp);
}

/*!
 * \brief read next values from profile
 */
void Bio::read_next_profile_value()
{
    P_gen = profile_scale_factor *
        component_profiles->get_profile_value(PROFILE_BIO_P, profile_id_p, t_next);
}

/*!
 * \brief set converter target power
 */
void Bio::pre_processing()
{
    if (P_ctrl > 0) {
        P_ctrl = 0;
    } else if (P_ctrl < -P_gen) {
        P_ctrl = -P_gen;
    }
    
    for (auto m : models) {
        if (m.first == TYPE_OPG_SYNGEN) {
            Syngen *syngen_temp = (Syngen*) m.second;
            syngen_temp->set_target_power(P_ctrl, Q_ctrl);
        }
    }
}

/*!
 * \brief calculate the power of all models
 */
void Bio::post_processing()
{
    for (auto m : models) {
        m.second->calculate_power();
    }
}

/*!
 * \brief get power generation
 * \return active power generation
 */
double Bio::get_generation()
{
    return P_gen;
}