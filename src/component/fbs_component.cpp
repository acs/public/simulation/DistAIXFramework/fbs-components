/**
 * This file belongs to DistAIX FBScomponents
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/

#include "component/fbs_component.h"

/*!
 * \brief Construct a new fbs component::fbs component object
 * \param _model_type   steady state or dynamic phasor
 * \param _v_nom        nominal voltage
 */
FBS_component::FBS_component(int _model_type, double _v_nom) 
    : v_nom(_v_nom), model_type(_model_type)
{
    t_next = 0;
}