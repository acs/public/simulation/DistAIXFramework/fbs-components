/**
 * This file belongs to DistAIX FBScomponents
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/

#include "component/transformer.h"
#include "fbs_config.h"
#include "model/dp/dp_line.h"
#include "model/sp/sp_line.h"

Transformer::Transformer(int _model_type, double _stepsize, double _v_nom, double _S_r,  double _R,
    double _X, double _ratio_nom, unsigned int _N, double _r)
    : FBS_component(_model_type, _v_nom), N(_N), r(_r)
{
    if (_model_type == MODEL_TYPE_DYNAMIC_PHASOR) {
        line = new DP_Line(_stepsize, _v_nom, _S_r, _R, _X, 0, 0);
    } else {
        /* default is static phasor */
        line = new SP_Line(_stepsize, _v_nom, _S_r, _R, _X, 0, 0);
    }
    ideal_transformer = new Ideal_transformer(_stepsize, _v_nom, _S_r, _ratio_nom);
    ratio_mul = 1.0;
}

Transformer::~Transformer() {
    delete ideal_transformer;
    delete line;
}

/*!
 * \brief set transformer turns ratio
 */
void Transformer::pre_processing()
{
    if (n_ctrl < -N) {
        n_ctrl = -N;
    } else if (n_ctrl > N) {
        n_ctrl = N;
    }
    ratio_mul = 1.0;
    if (N > 0) {
        ratio_mul = 1.0 + (n_ctrl * r) / (100.0 * N);
    }
    ideal_transformer->set_ratio_mul(ratio_mul);
    return;
}

double Transformer::get_ratio_mul()
{
    return ratio_mul;
}

/*!
 * \brief solve line and ideal transformer model
 * \param _v1_re [in]   input voltage
 * \param _v1_im [in]   input voltage
 * \param _i1_re [in]   input current
 * \param _i1_im [in]   input current
 * \param _v2_re [out]  output voltage
 * \param _v2_im [out]  output voltage
 * \param _i2_re [out]  output current
 * \param _i2_im [out]  output current
 */
void Transformer::solve(double _v1_re, double _v1_im, double _i1_re, double _i1_im, double &_v2_re,
    double &_v2_im, double &_i2_re, double &_i2_im)
{
    double vm_re, vm_im, im_re, im_im;
    line->solve(_v1_re, _v1_im, _i1_re, _i1_im, vm_re, vm_im, im_re, im_im);
    ideal_transformer->solve(vm_re, vm_im, -im_re, -im_im, _v2_re, _v2_im, _i2_re, _i2_im);
    return;
}

/*!
 * \brief calculate the input and output power of line
 */
void Transformer::post_processing()
{
    line->calculate_power();
    ideal_transformer->calculate_power();
    return;
}

/*!
 * \brief advance simulation for one time step
 * \param t next time step
 */
void Transformer::step(double t)
{
    t_next = t;
    line->step(t);
    ideal_transformer->step(t);
    return;
}

/*!
 * \brief set tap control value
 * \param _n_ctrl tap control value
 */
void Transformer::set_n_ctrl(int _n_ctrl)
{
    n_ctrl = _n_ctrl;
    return;
}

/*!
 * \brief get power
 * \param _P1 [out] active power input
 * \param _Q1 [out] reactive power input
 * \param _P1 [out] active power output
 * \param _Q1 [out] reactive power output
 */
void Transformer::get_power(double &_P1, double &_Q1, double &_P2, double &_Q2)
{
    double pm, qm;
    line->get_power(_P1, _Q1, pm, qm);
    ideal_transformer->get_power(pm, qm, _P2, _Q2);
    return;
}

int Transformer::get_n()
{
    return n_ctrl;
}


