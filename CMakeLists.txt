#############################################################################
#
# This file is part of DistAIX FBScomponents
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#############################################################################


cmake_minimum_required(VERSION 3.0)
project(fbs-components)

# create source list
file(GLOB_RECURSE SRC_LIST ./src/*.cpp)

#set compiler flags
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -g -O0 -std=c++11 -Wall")
set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} -g -O0 -std=c++11 -Wall")
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY "${CMAKE_CURRENT_BINARY_DIR}")

#find GNU scientific library (GSL)
find_package(GSL REQUIRED)

#create shared library
add_library(fbscomp SHARED ${SRC_LIST})

target_include_directories(fbscomp SYSTEM PUBLIC include)

#add additional libraries to link to (currently not required)
target_link_libraries(fbscomp ${GSL_LIBRARIES})

#additional definitions (currently NONE)
#add_definitions()
