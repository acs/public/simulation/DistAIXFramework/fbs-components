#############################################################################
# FBScomponents Ubuntu 18.04 Dockerfile
#
# This Dockerfile builds an image based on Ubuntu 18.04 which contains all dependencies
# to build FBScomponents
#
# This file belongs to DistAIX FBScomponents
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#############################################################################
FROM ubuntu:18.04

ARG GIT_REV=unknown
ARG GIT_BRANCH=unknown
ARG VERSION=unknown
ARG VARIANT=unknown

ENV DEBIAN_FRONTEND=noninteractive

# Toolchain and dependencies
RUN apt-get update && apt-get install -y \
	cmake g++ libgsl-dev

# set library environment variables
ENV LD_LIBRARY_PATH="/usr/local/lib:/usr/local/lib/x86_64-linux-gnu:/usr/local/lib64:/usr/lib"
ENV LIBRARY_PATH="/usr/local/lib:/usr/local/lib/x86_64-linux-gnu:/usr/local/lib64:/usr/lib"
ENV PATH="usr/local/bin:${PATH}"

WORKDIR /dbconnector
ENTRYPOINT bash

LABEL \
	org.label-schema.schema-version="1.0" \
	org.label-schema.name="FBScomponents" \
	org.label-schema.license="GPL-3.0" \
	org.label-schema.vcs-ref="$GIT_REV" \
	org.label-schema.vcs-branch="$GIT_BRANCH" \
	org.label-schema.version="$VERSION" \
	org.label-schema.variant="$VARIANT" \
	org.label-schema.vendor="Institute for Automation of Complex Power Systems, RWTH Aachen University" \
	org.label-schema.description="An image containing all build-time dependencies for FBScomponents based on Ubuntu" \
	org.label-schema.vcs-url="https://git.rwth-aachen.de/acs/public/simulation/DistAIXFramework/fbs-components"