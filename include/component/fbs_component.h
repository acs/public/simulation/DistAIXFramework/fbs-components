/**
 * This file belongs to DistAIX FBScomponents
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/

#ifndef FBS_COMPONENT
#define FBS_COMPONENT


/*! \brief Abstract base class for fbs components
 *
 * This class declares common interfaces for all fbs components.
 * */
class FBS_component {
public:
    FBS_component(int _model_type, double _v_nom);
    virtual ~FBS_component() = default;

    virtual void pre_processing() = 0;
    virtual void post_processing() = 0;
    virtual void step(double t) = 0;

protected:
    double v_nom;                   /*!< nominal voltage in V */
    int model_type;                 /*!< 0 for steady state, 1 for dynamic phasor */
    double t_next;                  /*!< Next simulation time step to simulate in seconds */
};

#endif //FBS_COMPONENT
