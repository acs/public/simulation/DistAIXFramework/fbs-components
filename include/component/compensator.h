/**
 * This file belongs to DistAIX FBScomponents
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/

#ifndef COMPENSATOR_H
#define COMPENSATOR_H

#include "component/prosumer.h"


/*! \brief class representing a Compensator
 * */
class Compensator : public Prosumer {
public:
    Compensator(int _model_type, double _stepsize, double _v_nom, double _S_r, unsigned int _N);
    ~Compensator() override = default;

    void pre_processing() override;
    void post_processing() override;

    void set_n_ctrl(unsigned int _n_ctrl);

protected:
    void read_next_profile_value() override;

    double B_r;             /*!< rated susceptance */
    unsigned int N;         /*!< number of discrete steps */
    unsigned int n_ctrl;    /*!< control value of compensator tap */
};

#endif //COMPENSATOR_H
