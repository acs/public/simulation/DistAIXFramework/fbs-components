/**
 * This file belongs to DistAIX FBScomponents
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/

#ifndef TRANSMISSION
#define TRANSMISSION

#include "component/fbs_component.h"
#include "model/line.h"


/*! \brief Class for transmission components
 * */
class Transmission : public FBS_component {
public:
    Transmission(int _model_type, double _stepsize, double _v_nom, double _S_r, double _R,
        double _X, double _G, double _B);
    ~Transmission() override;

    virtual void pre_processing();
    void solve(double _v1_re, double _v1_im, double _i1_re, double _i1_im, double &_v2_re,
        double &_v2_im, double &_i2_re, double &_i2_im);
    virtual void post_processing();
    virtual void step(double t) override;

    void get_power(double &_P1, double &_Q1, double &_P2, double &_Q2);
    void get_leackage_current(double &_i_leak_re, double &_i_leak_im);

protected:
    Line* line; /*!< line model */
};

#endif //TRANSMISSION
