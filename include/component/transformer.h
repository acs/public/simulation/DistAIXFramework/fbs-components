/**
 * This file belongs to DistAIX FBScomponents
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/

#ifndef TRANSFORMER
#define TRANSFORMER

#include "component/fbs_component.h"
#include "model/ideal_transformer.h"
#include "model/line.h"


/*! \brief Class for transformer components (composed of rx line and ideal transformer)
 * */
class Transformer : public FBS_component {
public:
    Transformer(int _model_type, double _stepsize, double _v_nom, double _S_r, double _R, double _X,
        double _ratio_nom, unsigned int _N, double _r);
    ~Transformer() override;

    void pre_processing() override;
    void solve(double _v1_re, double _v1_im, double _i1_re, double _i1_im, double &_v2_re,
        double &_v2_im, double &_i2_re, double &_i2_im);
    void post_processing() override;
    void step(double t) override;

    double get_ratio_mul();

    void set_n_ctrl(int _n_ctrl);
    int get_n();

    void get_power(double &_P1, double &_Q1, double &_P2, double &_Q2);

protected:
    Ideal_transformer* ideal_transformer;   /*!< pointer to ideal transformer model */
    int N;                                  /*!< number of tap positions (symmetrical) */
    double r;                               /*!< range in which turns ratio can be changed in % */
    int n_ctrl;                             /*!< control value for tap position */
    double ratio_mul;                       /*!< multiplicator for ratio */
    Line* line;                             /*!< line model */
};

#endif //TRANSFORMER
