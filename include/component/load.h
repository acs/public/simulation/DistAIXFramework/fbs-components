/**
 * This file belongs to DistAIX FBScomponents
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/

#ifndef LOAD
#define LOAD

#include "component/prosumer.h"
#include "interpolation.h"


/*! \brief class representing a load
 * */
class Load : public Prosumer {
public:
    Load(int _model_type, double _stepsize, double _v_nom, double _S_r, double _pf_min,
        Profile* profiles, int p_id, int q_id, double scale);
    ~Load() override = default;

    void pre_processing() override;
    void post_processing() override;

    void get_demand(double &_P_dem, double &_Q_dem);

protected:
    void read_next_profile_value() override;

    int profile_id_p;               /*!< ID of active power demand profile */
    int profile_id_q;               /*!< ID of reactive power demand profile */
    double profile_scale_factor;    /*!< scaling of profile values */
    double P_demand;                /*!< active power demand in W */
    double Q_demand;                /*!< reactive power demand in var */
};

#endif //LOAD
