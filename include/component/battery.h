/**
 * This file belongs to DistAIX FBScomponents
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/

#ifndef BATTERY_H
#define BATTERY_H

#include "component/prosumer.h"
#include "model/storage.h"


/*! \brief class representing a battery
 * */
class Battery : public Prosumer {
public:
    Battery(int _model_type, double _stepsize, double _v_nom, double _S_r, double _pf_min, double _C, double _P_max,
        double _soc_init);
    ~Battery() override;

    void pre_processing() override;
    void post_processing() override;
    void step(double t) override;

    double get_soc();
    double get_energy();
    
protected:
    void read_next_profile_value() override;

    Storage* storage;    /*! storage */
};

#endif //BATTERY_H
