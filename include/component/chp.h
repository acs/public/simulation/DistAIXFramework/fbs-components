/**
 * This file belongs to DistAIX FBScomponents
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/

#ifndef CHP_H
#define CHP_H

#include "component/prosumer.h"
#include "model/storage.h"


/*! \brief class representing a chp
 * */
class CHP : public Prosumer {
public:
    CHP(int _model_type, double _stepsize, double _v_nom, double _S_r, double _pf_min, double _P_nom,
        double _f_el, double _P_sec, double _f_el_sec, double _C, double _P_max, double _soc_init,
        Profile* profiles, int th_id1, int th_id2, double scale, double scale_ww);
    ~CHP() override;

    void pre_processing() override;
    void post_processing() override;
    void step(double t) override;

    double get_soc();
    double get_energy();
    double get_th_demand();
    double get_P_sec();

protected:
    void read_next_profile_value() override;

    double f_el;                    /*!< conversion from el to th */
    double f_el_sec;                    /*!< conversion from el to th for secondary heater*/
    double P_max_syngen;            /*!< maximum power of syngen (needed for sec heater) */
    double P_sec   ;                /*!< maximum power of sec heater*/
    int profile_id_th1;             /*!< ID of thermal demand profile */
    int profile_id_th2;             /*!< ID of second thermal demand profile */
    double profile_scale_factor;    /*!< scaling of profile values */
    double profile_scale_factor_ww; /*!< scaling of profile values */
    double th_demand;               /*!< thermal power demand in W */
    double P_ctrl_sec_heater;       /*!< el. power consumed by secondary heater */
    Storage* storage;               /*!< storage */
};

#endif //CHP_H
