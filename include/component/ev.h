/**
 * This file belongs to DistAIX FBScomponents
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/

#ifndef EV_H
#define EV_H

#include "component/prosumer.h"
#include "model/storage.h"


/*! \brief class representing a EV
 * */
class EV : public Prosumer {
public:
    EV(int _model_type, double _stepsize, double _v_nom, double _S_r, double _pf_min, double _C,
        double _P_max, double _soc_init, Profile* profiles, int conn_id, int p_id, double scale);
    ~EV() override;

    void pre_processing() override;
    void post_processing() override;
    void step(double t) override;

    double get_soc();
    double get_energy();
    double get_t_connected();
    bool get_connected();
    void get_demand(double &_P_dem, double &_Q_dem);

protected:
    void read_next_profile_value() override;

    int profile_id_conn;            /*!< ID of connection profile */
    int profile_id_p;               /*!< ID of consumption profile */
    double profile_scale_factor;    /*!< scaling of profile values */
    double t_conn;                  /*!< time until disconnection */
    double t_prev;                  /*!< previous time step */
    bool connected;                 /*!< indicates if ev is connected to grid */
    bool connected_prev;            /*!< indicates if ev was connected during last time step */
    double P_con;                   /*!< power consumption when not driving */
    double P_demand;		    /*!< power demand of non-flexible EV */
    double Q_demand;		    /*!< power demand of non-flexible EV */
    Storage* storage;               /*! storage */
};

#endif //EV_H
