/**
 * This file belongs to DistAIX FBScomponents
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/

#ifndef PROSUMER
#define PROSUMER

#include "component/fbs_component.h"
#include "model/one_port_grounded.h"
#include "profile.h"
#include <vector>


/*! \brief Abstract base class for prosumer components
 *
 * This class declares common interfaces for all prosumer components.
 * */
class Prosumer : public FBS_component {
public:
    Prosumer(int _model_type, double _v_nom, Profile* profiles);
    ~Prosumer() override;

    void solve(double _v_re, double _v_im, double &_i_re, double &_i_im);
    void step(double t) override;

    void get_power(double &_P, double &_Q);
    void get_current(double &_i_re, double &_i_im);
    double get_pf();
    void set_power_ctrl(double _P_ctrl, double _Q_ctrl);

protected:
    virtual void read_next_profile_value() = 0;
    Profile* component_profiles;

    std::vector<std::pair<int, One_port_grounded*>> models; /*!< type and pointer of all models */
    double P_ctrl;                                  /*!< control value of active power in W*/
    double Q_ctrl;                                  /*!< control value of reactive power in var*/
};

#endif //PROSUMER
