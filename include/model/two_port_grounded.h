/**
 * This file belongs to DistAIX FBScomponents
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/

#ifndef TWO_PORT_GROUNDED
#define TWO_PORT_GROUNDED

#include "model/fbs_model.h"


/*! \brief Abstract base class for models with two ports.
 *
 * This class declares common interfaces for all two port models.
 * */
class Two_port_grounded : public FBS_model {
public:
    Two_port_grounded(double _stepsize, double _v_nom, double _S_r);
    ~Two_port_grounded() override = default;

    virtual void solve(double _v1_re, double _v1_im, double _i1_re, double _i1_im, double &_v2_re,
        double &_v2_im, double &_i2_re, double &_i2_im) = 0;
    void calculate_power();
    void get_power(double &_P1, double &_Q1, double &_P2, double &_Q2);

protected:
    double v1_re;       /*!< real part of input voltage in V */
    double v1_im;       /*!< imaginary part of input voltage in V */
    double i1_re;       /*!< real part of input current in A */
    double i1_im;       /*!< imaginary part of input current in A */
    double P1;          /*!< active input power in W */
    double Q1;          /*!< reactive input power in var */
    double v2_re;       /*!< real part of output voltage in V */
    double v2_im;       /*!< imaginary part of output voltage in V */
    double i2_re;       /*!< real part of output current in A */
    double i2_im;       /*!< imaginary part of output current in A */
    double P2;          /*!< active output power in W */
    double Q2;          /*!< reactive output power in var */
};

#endif //TWO_PORT_GROUNDED
