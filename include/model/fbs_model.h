/**
 * This file belongs to DistAIX FBScomponents
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/

#ifndef FBS_MODEL
#define FBS_MODEL


/*! \brief Abstract base class for fbs models
 *
 * This class declares common interfaces for all fbs models.
 * */
class FBS_model {
public:
    FBS_model(double _stepsize, double _v_nom, double _S_r);
    virtual ~FBS_model() = default;

    virtual void step(double t) = 0;

protected:
    bool first_step;    /*!< marks the first run of step, so constant values could be added
                         * properly */
    double t_next;      /*!< Next simulation time step to simulate in seconds */
    double t_prev;      /*!< Previous simulation time step to simulate in seconds */
    double stepsize;    /*!< step size (has to be calculated in step function) */
    double v_nom;       /*!< nominal voltage in V */
    double S_r;         /*!< rated power in VA */
    double f_nom;       /*!< Nominal frequency*/
    double omega_nom;   /*!< Omega = 2 * pi * fnom*/
};

#endif //FBS_COMPONENT
