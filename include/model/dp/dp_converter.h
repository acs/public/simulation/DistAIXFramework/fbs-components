/**
 * This file belongs to DistAIX FBScomponents
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/

#ifndef DP_CONVERTER
#define DP_CONVERTER

#include "model/converter.h"

#define N_EQN 6


/*! \brief class for dynamic phasor converter model
 * */
class DP_converter : public Converter {
public:
    DP_converter(double _stepsize, double vnom, double _S_r, double _pf_min);
    ~DP_converter() override = default;

    void step(double t) override;
    void solve(double _v_re, double _v_im, double &_i_re, double &_i_im) override;

protected:
    void init();

    void partial_pivoting();
    void LU_decomposition();
    void set_A();
    void set_B();

    double L_1 = 0.01;          /*!< first inductance of LCL Filter */
    double L_2 = 0.02;          /*!< second inductance of LCL Filter */
    double C = 3e-6;            /*!< capacitance of LCL Filter */
    double K_r = 3200;          /*!< K_r value of PR controller */
    double K_p = 1.4;           /*!< K_p value of PR controller */

    double A_re[N_EQN*N_EQN];   /*!< real part of Matrix A in: A*X = B */
    double A_im[N_EQN*N_EQN];   /*!< imaginary part of Matrix A in: A*X = B */
    double B_re[N_EQN];         /*!< real part of Vector B in: A*X = B */
    double B_im[N_EQN];         /*!< real part of Vector B in: A*X = B */
    double pm[N_EQN*N_EQN];     /*!< permutation matrix */
    double x_re[N_EQN];         /*!< real part of result vector (x[0]: V_2, x[1]: I_2, x[2]: V_C, 
                                 * x[3]: I_1, x[4]: X_1, x[5]: X_2) */
    double x_im[N_EQN];         /*!< imaginary part of result vector */
    double x_temp_re[N_EQN];    /*!< real part of temporary result vector 
                                 * (used during iterations within one time step) */
    double x_temp_im[N_EQN];    /*!< imaginary part of temporary result vector */
};

#endif //DP_CONVERTER
