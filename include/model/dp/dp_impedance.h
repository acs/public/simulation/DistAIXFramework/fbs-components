/**
 * This file belongs to DistAIX FBScomponents
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/

#ifndef DP_IMPEDANCE
#define DP_IMPEDANCE

#include "model/impedance.h"


/*! \brief class for dynamic phasor impedance model
 * */
class DP_impedance : public Impedance {
public:
    DP_impedance(double _stepsize, double _v_nom, double _S_r);
    ~DP_impedance() override = default;

    void step(double t) override;
    void solve(double _v_re, double _v_im, double &_i_re, double &_i_im) override;

protected:
    void init();
    double i_prev_re;   /*!< current from previous time step */
    double i_prev_im;   /*!< current from previous time step */
};

#endif //DP_IMPEDANCE
