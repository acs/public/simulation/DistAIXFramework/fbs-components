/**
 * This file belongs to DistAIX FBScomponents
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/

#ifndef DP_LINE
#define DP_LINE

#include "model/line.h"


/*! \brief class for dynamic phasor pi line model
 * */
class DP_Line : public Line {
public:
    DP_Line(double _stepsize, double _v_nom, double _S_r, double _R, double _X, double _G, double _B);
    ~DP_Line() override = default;

    void step(double t) override;
    void solve(double _v1_re, double _v1_im, double _i1_re, double _i1_im, double &_v2_re,
        double &_v2_im, double &_i2_re, double &_i2_im) override;

protected:
    void init();
    double i_rx_re;         /*!< real part of current through rx element */
    double i_rx_im;         /*!< imaginary part of current through rx element */
    double v1_prev_re;      /*!< v1 from previous time step */
    double v1_prev_im;      /*!< v1 from previous time step */
    double v2_prev_re;      /*!< v2 from previous time step */
    double v2_prev_im;      /*!< v2 from previous time step */
    double i_rx_prev_re;    /*!< i_rx from previous time step */
    double i_rx_prev_im;    /*!< i_rx from previous time step */
};

#endif //DP_LINE
