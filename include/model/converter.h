/**
 * This file belongs to DistAIX FBScomponents
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/

#ifndef CONVERTER
#define CONVERTER

#include "model/one_port_grounded.h"


/*! \brief class for converter model
 * */
class Converter : public One_port_grounded {
public:
    Converter(double _stepsize, double _v_nom, double _S_r, double _pf_min);
    ~Converter() override = default;

    virtual void set_target_power(double &_P_target, double _Q_target);

protected:
    double pf_min;      /*!< minimum power factor */
    double P_target;    /*!< target active power in W */
    double Q_target;    /*!< target reactive power in var */
};

#endif //CONVERTER
