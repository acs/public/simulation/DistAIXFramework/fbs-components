/*
 * This file belongs to DistAIX FBScomponents
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/

#ifndef ONE_PORT_GROUNDED
#define ONE_PORT_GROUNDED

#include "model/fbs_model.h"


/*! \brief Abstract base class for models with one port (prosumers)
 *
 * This class declares common interfaces for all one port models.
 * */
class One_port_grounded : public FBS_model {
public:
    One_port_grounded(double _stepsize, double _v_nom, double _S_r);
    ~One_port_grounded() override = default;

    virtual void solve(double _v_re, double _v_im, double &_i_re, double &_i_im) = 0;
    void calculate_power();
    void get_power(double &_P, double &_Q);
    void get_current(double &_i_re, double &_i_im);

protected:
    double v_re;    /*!< real part of voltage in V */
    double v_im;    /*!< imaginary part of voltage in V */
    double i_re;    /*!< real part of current in A */
    double i_im;    /*!< imaginary part of current in A */
    double P;       /*!< active power in W */
    double Q;       /*!< reactive power in var */
};

#endif //ONE_PORT_GROUNDED
