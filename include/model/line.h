/**
 * This file belongs to DistAIX FBScomponents
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/

#ifndef LINE
#define LINE

#include "model/two_port_grounded.h"


/*! \brief Abstract base class for line models.
 * */
class Line : public Two_port_grounded {
public:
    Line(double _stepsize, double _v_nom, double _S_r, double _R, double _X, double _G, double _B);
    ~Line() override = default;

    void get_leakage_current(double &_i_leak_re, double &_i_leak_im);

protected:
    double R;               /*!< Resistance of pi line in Ohm */
    double X;               /*!< Reactance of pi line in Ohm */
    double G;               /*!< Conductance of pi line in 1/Ohm */
    double B;               /*!< Susceptance of pi line in 1/Ohm */
    double i1_leak_re;      /*!< real part of input leakage current in A */
    double i1_leak_im;      /*!< imaginary part of input leakage current in A */
    double i2_leak_re;      /*!< real part of output leakage current in A */
    double i2_leak_im;      /*!< imaginary part of output leakage current in A */
};

#endif //LINE
