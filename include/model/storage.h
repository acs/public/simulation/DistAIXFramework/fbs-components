/**
 * This file belongs to DistAIX FBScomponents
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/

#ifndef STORAGE
#define STORAGE

#include "model/fbs_model.h"


/*! \brief Abstract base class for storages
 * */
class Storage : public FBS_model {
public:
    Storage(double _stepsize, double _C, double _P_max_in, double _P_max_out, double _soc_init);
    ~Storage() override = default;

    void step(double t) override;

    void update(double P);
    double get_energy();
    double get_energy_lack();
    double get_soc();
    void get_power_limits(double &P_min, double &P_max);

protected:
    double C;           /*!< storage capacity in Wh */
    double P_max_in;    /*!< maximum charging power */
    double P_max_out;   /*!< maximum discharging power */
    double soc;         /*!< state of charge */
};

#endif //STORAGE
