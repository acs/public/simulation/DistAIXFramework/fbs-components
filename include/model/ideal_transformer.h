/**
 * This file belongs to DistAIX FBScomponents
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/

#ifndef IDEAL_TRANSFORMER
#define IDEAL_TRANSFORMER

#include "model/two_port_grounded.h"


/*! \brief Class for ideal transformer model.
 * */
class Ideal_transformer : public Two_port_grounded {
public:
    Ideal_transformer(double _stepsize, double _v_nom, double _S_r, double _ratio_nom);
    ~Ideal_transformer() override = default;

    void step(double t) override;
    void solve(double _v1_re, double _v1_im, double _i1_re, double _i1_im, double &_v2_re,
        double &_v2_im, double &_i2_re, double &_i2_im) override;

    void set_ratio_mul(double ratio_mul);

protected:
    double ratio_nom;   /*!< nominal ratio */
    double ratio;       /*!< ratio of transformer */
};

#endif //IDEAL_TRANSFORMER
