/**
 * This file belongs to DistAIX FBScomponents
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/

#ifndef SP_CONVERTER
#define SP_CONVERTER

#include "model/converter.h"


/*! \brief class for static phasor converter model
 * */
class SP_converter : public Converter {
public:
    SP_converter(double _stepsize, double _v_nom, double _S_r, double _pf_min);
    ~SP_converter() override = default;

    void step(double t) override;
    void solve(double _v_re, double _v_im, double &_i_re, double &_i_im) override;
};

#endif //SP_CONVERTER
