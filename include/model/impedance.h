/**
 * This file belongs to DistAIX FBScomponents
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/

#ifndef IMPEDANCE
#define IMPEDANCE

#include "model/one_port_grounded.h"


/*! \brief base class for impedance model
 * */
class Impedance : public One_port_grounded {
public:
    Impedance(double _stepsize, double _v_nom, double _S_r);
    ~Impedance() override = default;

    void set_impedance_value(double _Z_re, double _Z_im);

protected:
    double Z_re;    /*!< real part of impedance in Ohm */
    double Z_im;    /*!< imaginary part of impedance in Ohm */
};

#endif //IMPEDANCE
