/**
 * This file belongs to DistAIX FBScomponents
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/

#define EPSILON 0.000001

/*defines for model types*/
#define MODEL_TYPE_STEADY_STATE 0
#define MODEL_TYPE_DYNAMIC_PHASOR 1

/*definition one port grounded type mappings*/
#define TYPE_OPG_IMPEDANCE  0
#define TYPE_OPG_CONVERTER  1
#define TYPE_OPG_SYNGEN     2