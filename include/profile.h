/**
 * This file belongs to DistAIX FBScomponents
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/

#ifndef PROFILE
#define PROFILE

#include "interpolation.h"
#include <vector>

#define PROFILE_LOAD_P  1
#define PROFILE_LOAD_Q  2
#define PROFILE_PV_P    3
#define PROFILE_WEC_P   4
#define PROFILE_TH      5
#define PROFILE_EV_CONN 6
#define PROFILE_EV_P    7
#define PROFILE_BIO_P   8

/*! \brief Profile class
 *
 * This class can be used to access profile values
*/
class Profile{
public:
    Profile();
    ~Profile();

    void init(std::string interp_type);

    void add_profile(int profile_type, double* t, double *v, unsigned long int number_of_values,
        std::string header);
    double get_profile_value(int profile_type, unsigned int id, double t);
    double get_max_time(int profile_type, unsigned int id);

private:
    std::string interpolation_type;
    std::vector<Interpolation*> load_P_profiles;
    std::vector<Interpolation*> load_Q_profiles;
    std::vector<Interpolation*> pv_P_profiles;
    std::vector<Interpolation*> wec_P_profiles;
    std::vector<Interpolation*> th_profiles;
    std::vector<Interpolation*> ev_conn_profiles;
    std::vector<Interpolation*> ev_P_profiles;
    std::vector<Interpolation*> bio_P_profiles;
};

#endif //PROFILE
