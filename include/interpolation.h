/**
 * This file belongs to DistAIX FBScomponents
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/

#ifndef INTERPOLATION
#define INTERPOLATION

#include <gsl/gsl_interp.h>
#include <string>


/*! \brief Interpolation class
 *
 * This class can be used to interpolate input profiles of agents. Currently, it offers the interpolation types "linear" and "hold".
*/
class Interpolation{
    
private:
    double *data;                       //!< Profile input values
    double *time_values;                //!< Time values of the input profile
    unsigned int number_values;         //!< Number of values in the input profile
    double max_time_value;              //!< largest time value (in sec) contained in time_values
    gsl_interp *profile_interpolated;   //!< Interpolation using GNU Scientific Library (GSL)
    gsl_interp_accel * accelerator;     //!< GSL accelerator used for interpolation
    std::string interpolation_type;     //!< selected type of interpolation, can be "hold" or "linear"
    unsigned int current_position;      //!< the current position in the input profile
    std::string header;                 /*!< information about data */

public:
    Interpolation(std::string type, double *t, double *v, int _number_values, std::string &_header);
    ~Interpolation() = default;
    double get_interpolated_value(double time_step);
    double get_max_time();
};

#endif
